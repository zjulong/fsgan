import  torch, math
import  numpy as np
import  torch.nn as nn
import  torch.optim as optim
from    torch.utils.data import DataLoader, TensorDataset
from    torchvision import datasets, transforms
import  visdom
from    torch.nn import functional as F



class Generator(nn.Module):

	def __init__(self, z_dim, c_dim):
		super(Generator, self).__init__()

		self.model = nn.Sequential(
			nn.Linear(z_dim + c_dim, 128),
			nn.BatchNorm1d(128),
			nn.LeakyReLU(0.2),

			nn.Linear(128, 256),
			nn.BatchNorm1d(256),
			nn.LeakyReLU(0.2),

			nn.Linear(256, 512),
			nn.BatchNorm1d(512),
			nn.LeakyReLU(0.2),

			nn.Linear(512, 1024),
			nn.BatchNorm1d(1024),
			nn.LeakyReLU(0.2),

			nn.Linear(1024, 28 * 28),
			nn.Tanh()

		)


	def forward(self, z, c):
		"""

		:param z: [b, z]
		:param c: [b, c]
		:return:
		"""
		x = torch.cat([z, c], dim=1)

		x = self.model(x)

		x = x.view(-1, 1, 28, 28)

		return x

	def init_weight(self, m):

		if type(m) == nn.Linear:
			nn.init.xavier_uniform_(m.weight)
			m.bias.data.fill_(0.01)
			# with torch.no_grad():
			# 	stdv = 1. / math.sqrt(m.weight.size(1))
			# 	m.weight.uniform_(-stdv, stdv)
			# 	m.bias.uniform_(-stdv, stdv)
			print('init.')

		elif type(m) == nn.BatchNorm1d:
			# with torch.no_grad():
			# 	m.weight.uniform_()
			# 	m.bias.zero_()
			pass





class Discriminator(nn.Module):

	def __init__(self):
		super(Discriminator, self).__init__()

		self.model = nn.Sequential(
			nn.Linear(2 * 28 * 28, 512),
			nn.LeakyReLU(0.2),

			nn.Linear(512, 256),
			nn.LeakyReLU(0.2),

			nn.Linear(256, 1),
			nn.Sigmoid()

		)

	def forward(self, x, y):
		"""

		:param x: [b, 2, 28, 28]
		:param y: [b]
		:return:
		"""
		# [b] => [b, 1, 28, 28]
		y = y.unsqueeze(1).unsqueeze(2).unsqueeze(3).expand(y.size(0), 1, 28, 28)
		# print('xx', x.shape, y.shape)
		# concat
		x = torch.cat([x, y], dim=1)
		# flatten
		x = x.view(x.size(0), -1)
		# => [b]
		x = self.model(x)
		return x

	def init_weight(self, m):

		if type(m) == nn.Linear:
			nn.init.xavier_uniform_(m.weight)
			m.bias.data.fill_(0.01)
			print('init2.')

class Generator2:

	def __init__(self, z_dim, c_dim, device):

		# according to Pytorch w/b format, w = [out_dim, in_dim]
		# b = [out_dim]
		self.vars = [
			# [z+c, 128]
			torch.ones(128, z_dim + c_dim, requires_grad=True).to(device),
			torch.zeros(128, requires_grad=True).to(device),
			torch.rand(128, requires_grad=True).to(device),
			torch.zeros(128, requires_grad=True).to(device),
			# [128, 256]
			torch.ones(256, 128, requires_grad=True).to(device),
			torch.zeros(256, requires_grad=True).to(device),
			torch.rand(256, requires_grad=True).to(device),
			torch.zeros(256, requires_grad=True).to(device),
			# [256, 512]
			torch.ones(512, 256, requires_grad=True).to(device),
			torch.zeros(512, requires_grad=True).to(device),
			torch.rand(512, requires_grad=True).to(device),
			torch.zeros(512, requires_grad=True).to(device),
			# [512, 1024]
			torch.ones(1024, 512, requires_grad=True).to(device),
			torch.zeros(1024, requires_grad=True).to(device),
			torch.rand(1024, requires_grad=True).to(device),
			torch.zeros(1024, requires_grad=True).to(device),
			# [1024, 28*28]
			torch.ones(28 * 28, 1024, requires_grad=True).to(device),
			torch.zeros(28 * 28, requires_grad=True).to(device),
			torch.rand(28 * 28, requires_grad=True).to(device),
			torch.zeros(28 * 28, requires_grad=True).to(device),
		]

		# moving mean and variance for normalization
		# no gradients needed.
		self.bns = [
			torch.zeros(128).to(device),
			torch.ones(128).to(device),

			torch.zeros(256).to(device),
			torch.ones(256).to(device),

			torch.zeros(512).to(device),
			torch.ones(512).to(device),

			torch.zeros(1024).to(device),
			torch.ones(1024).to(device),

			torch.zeros(28 * 28).to(device),
			torch.ones(28 * 28).to(device),

		]


	def init_weight(self, vars=None):
		"""
		init vars and self.bns
		:param vars:
		:return:
		"""

		if vars is None:
			vars = self.vars

		vars_idx = 0

		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4

		# zero mean and one variance.
		for i in range(len(self.bns) // 2 ):
			self.bns[i].zero_()
			self.bns[2 * i + 1].fill_(1)


	def forward(self, z, c, vars):
		"""

		:param z:
		:param c:
		:param vars:
		:return:
		"""

		vars_idx, bns_idx = 0, 0

		# [b, z_dim] + [b, c_dim] => [b, new_dim]
		x = torch.cat([z, c], dim=1)

		# [b, z+c] => [b, 128]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.8)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 128] => [b, 256]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.8)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 256] => [b, 512]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.8)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 512] => [b, 1024]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.8)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 1024] => [b, 28*28]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.8)

		x = F.tanh(x)
		vars_idx += 4
		bns_idx += 2

		# reshape
		x = x.view(-1, 1, 28, 28)

		return x


class Discriminator2:

	def __init__(self, n_class, device):

		# according to Pytorch w/b format, w = [out_dim, in_dim]
		# b = [out_dim]
		self.vars = [
			# [28*28, 512]
			torch.ones(512, 2 * 28 * 28, requires_grad=True).to(device),
			torch.zeros(512, requires_grad=True).to(device),
			# [512, 256]
			torch.ones(256, 512, requires_grad=True).to(device),
			torch.zeros(256, requires_grad=True).to(device),
			# [256, n]
			torch.ones(n_class, 256, requires_grad=True).to(device),
			torch.zeros(n_class, requires_grad=True).to(device)
		]





	def init_weight(self, vars=None):

		if vars is None:
			vars = self.vars

		vars_idx = 0

		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		vars_idx += 2


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		vars_idx += 2


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		vars_idx += 2




	def forward(self, x, vars):
		"""

		:param x: [b, 1, 28, 28]
		:param vars:
		:return:
		"""

		vars_idx = 0

		# [b, 1/2, 28, 28]
		x = x.view(x.size(0), -1)

		# [b, 28*28] => [b, 512]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		# x = self.bn1(x)
		x = F.leaky_relu(x, 0.2)
		vars_idx += 2

		# [b, 512] => [b, 256]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		# x = self.bn2(x)
		x = F.leaky_relu(x, 0.2)
		vars_idx += 2


		# [b, 256] => [b, n_class]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		# x = self.bn3(x)
		# here follow by CrossEntroyLoss
		# x = F.leaky_relu(x, 0.2)
		x = F.sigmoid(x)
		vars_idx += 2

		return x



def gan_main():

	from mnist_class import MNIST

	lr_d = 2e-4
	lr_g = 2e-4
	imagesz = 28
	batchsz1 = 64 # for fake data
	batchsz2 = 64 # for real data
	z_dim = 100
	n_class = 10

	device = torch.device('cuda')
	vis = visdom.Visdom()


	transform = transforms.Compose([transforms.Resize([imagesz, imagesz]),
	                                transforms.ToTensor(),
	                                transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))])
	# use self defined MNIST
	mnist = MNIST('data/mnist', class_idx=range(n_class), train=True, download=True, transform=transform)

	db = DataLoader(mnist, batch_size=batchsz2, shuffle=True)
	db_iter = iter(db)

	c_dist = torch.distributions.categorical.Categorical(probs=torch.tensor([1/n_class] * n_class))

	g = Generator(z_dim, n_class).to(device)
	d = Discriminator().to(device)
	for p in g.parameters():
		print(type(p))
	g_optim = optim.Adam(g.parameters(), lr=2e-4)
	d_optim = optim.Adam(d.parameters(), lr=2e-4)
	criteon = nn.MSELoss(size_average=True).to(device=device)

	# g.apply(g.init_weight)
	# d.apply(d.init_weight)


	for epoch in range(1000000):

		# [b, z]
		z = torch.rand(batchsz1, z_dim).to(device)
		# [b] => [b, 1]
		y_hat = c_dist.sample((batchsz1,) ).unsqueeze(1)
		# [b, 1] => [b, n_class]
		y_hat_oh = torch.zeros(batchsz1, n_class).scatter_(1, y_hat, 1)
		# [b, 1] => [b]
		y_hat, y_hat_oh = y_hat.squeeze(1).to(device).float(), y_hat_oh.to(device).float()
		# print(y_hat, y_hat_oh)

		# [b, z+c] => [b, 1, 28, 28]
		x_hat = g(z, y_hat_oh)
		y_fake = torch.zeros(x_hat.size(0)).to(device)

		# [b, 1, 28, 28], [b]
		try:
			x, y = next(db_iter)
		except StopIteration as err:
			db_iter = iter(db)
			x, y = next(db_iter)
		x, y = x.to(device), y.to(device).float()
		y_real = torch.ones(y.size(0)).to(device)

		# 1. train D
		# forward
		pred_real = d(x, y)
		pred_fake = d(x_hat, y_hat)
		# compute real sample loss, [b] => [b, 1]
		loss_real = criteon(pred_real, y_real.unsqueeze(1))
		loss_fake = criteon(pred_fake, y_fake.unsqueeze(1))
		loss_d = ( loss_real + loss_fake ) / 2
		# backward
		# d_grads = torch.autograd.grad(loss_d, d.parameters())
		# with torch.no_grad():
		# 	for p, grad in zip(d.parameters(), d_grads):
		# 		p -= lr_d * grad
		d_optim.zero_grad()
		loss_d.backward()
		d_optim.step()


		# 2. train G
		# [b, z]
		z = torch.rand(batchsz1, z_dim).to(device)
		# [b] => [b, 1]
		y_hat = c_dist.sample((batchsz1,) ).unsqueeze(1)
		# [b, 1] => [b, n_class]
		y_hat_oh = torch.zeros(batchsz1, n_class).scatter_(1, y_hat, 1)
		# [b, 1] => [b]
		y_hat, y_hat_oh = y_hat.squeeze(1).to(device).float(), y_hat_oh.to(device).float()
		# print(y_hat, y_hat_oh)

		# forward
		# [b, z+c] => [b, 1, 28, 28]
		x_hat = g(z, y_hat_oh)
		# concat [b, 1, 28, 28] => [b, 2, 28, 28]
		y_real = torch.ones(x_hat.size(0)).to(device)
		pred_real = d(x_hat, y_hat)
		loss_g = criteon(pred_real, y_real.unsqueeze(1))
		# backward
		# g_grads = torch.autograd.grad(loss_g, g.parameters())
		# with torch.no_grad():
		# 	for p, grad in zip(g.parameters(), g_grads):
		# 		p -= lr_d * grad

		g_optim.zero_grad()
		loss_g.backward()
		g_optim.step()






		if epoch % 500 == 0:

			# de-normalize, [b, 1, 28, 28]
			x_hat = x_hat[:20]
			# [b, 1, 28, 28]
			y_hat = y_hat[:20]
			x_hat = x_hat * 0.5 + 0.5
			# print(x_hat[0,0,2])
			x_hat = F.upsample(x_hat, scale_factor=3)
			vis.images(x_hat, nrow=5, win='cgan-mnist-x-hat', opts={'caption':'cgan-mnist-x-hat'})
			vis.text(str(y_hat.detach().cpu().numpy()), win='cgan-mnist-y-hat', opts={'caption':'cgan-mnist-y-hat'})
			# print(pred[:20])
			# print(y[:20])

			x = x[:20]
			x = x * 0.5 + 0.5
			x = F.upsample(x, scale_factor=3)
			vis.images(x, nrow=5, win='cgan-mnist-x', opts={'caption':'cgan-mnist-x'})


			print('>>', epoch, loss_d, loss_g)



			# print('D grads:', end=' ')
			# for p in d_grads:
			# 	print('%.4f'%p.norm().item(), end=' ')
			# print(' G grads:')
			# for p in g_grads:
			# 	print('%.4f'%p.norm().item(), end=' ')
			# print('')

			print('D weight:', end=' ')
			for p in d.parameters():
				print('%.4f' % p.norm().item(), end=' ')
			print(' G weight:')
			for p in d.parameters():
				print('%.4f' % p.norm().item(), end=' ')
			print('')







if __name__ == '__main__':
	import argparse
	args = argparse.ArgumentParser()
	args.add_argument('-g', action='store_true', help='use gan to train')
	args = args.parse_args()

	gan_main()

