import  torch, time, os, pickle
import  numpy as np
import  torch.nn as nn
import  torch.optim as optim
from    torch.utils.data import DataLoader
from    torchvision import datasets, transforms
import  visdom
from    torch.nn import functional as F



class Generator(nn.Module):
	# Network Architecture is exactly same as in infoGAN (https://arxiv.org/abs/1606.03657)
	# Architecture : FC1024_BR-FC7x7x128_BR-(64)4dc2s_BR-(1)4dc2s_S
	def __init__(self, input_dim=100, output_dim=1, input_size=32, class_num=10):
		"""

		:param input_dim: z-dim
		:param output_dim: img size
		:param input_size: img size
		:param class_num:
		"""
		super(Generator, self).__init__()

		self.input_dim = input_dim
		self.output_dim = output_dim
		self.input_size = input_size
		self.class_num = class_num

		self.fc = nn.Sequential(
			nn.Linear(self.input_dim + self.class_num, 1024),
			nn.BatchNorm1d(1024),
			nn.ReLU(),
			nn.Linear(1024, 128 * (self.input_size // 4) * (self.input_size // 4)),
			nn.BatchNorm1d(128 * (self.input_size // 4) * (self.input_size // 4)),
			nn.ReLU(),
		)
		self.deconv = nn.Sequential(
			nn.ConvTranspose2d(128, 64, 4, 2, 1),
			nn.BatchNorm2d(64),
			nn.ReLU(),
			nn.ConvTranspose2d(64, self.output_dim, 4, 2, 1),
			nn.Tanh(),
		)
		# utils.initialize_weights(self)

	def forward(self, input, label):
		"""

		:param input: [b, z-dim]
		:param label:
		:return:
		"""
		x = torch.cat([input, label], 1)
		x = self.fc(x)
		x = x.view(-1, 128, (self.input_size // 4), (self.input_size // 4))
		x = self.deconv(x)

		return x


class Discriminator(nn.Module):
	# Network Architecture is exactly same as in infoGAN (https://arxiv.org/abs/1606.03657)
	# Architecture : (64)4c2s-(128)4c2s_BL-FC1024_BL-FC1_S
	def __init__(self, input_dim=1, output_dim=1, input_size=32, class_num=10):
		"""

		:param input_dim: image channel
		:param output_dim: 1
		:param input_size: image size
		:param class_num:
		"""
		super(Discriminator, self).__init__()

		self.input_dim = input_dim
		self.output_dim = output_dim
		self.input_size = input_size
		self.class_num = class_num

		self.conv = nn.Sequential(
			nn.Conv2d(self.input_dim + self.class_num, 64, 4, 2, 1),
			nn.LeakyReLU(0.2),
			nn.Conv2d(64, 128, 4, 2, 1),
			nn.BatchNorm2d(128),
			nn.LeakyReLU(0.2),
		)
		self.fc = nn.Sequential(
			nn.Linear(128 * (self.input_size // 4) * (self.input_size // 4), 1024),
			nn.BatchNorm1d(1024),
			nn.LeakyReLU(0.2),
			nn.Linear(1024, self.output_dim),
			nn.Sigmoid(),
		)
		# utils.initialize_weights(self)

	def forward(self, input, label):
		"""

		:param input: [b, c, h, w]
		:param label: [b, label, h, w]
		:return:
		"""
		x = torch.cat([input, label], 1)
		x = self.conv(x)
		x = x.view(-1, 128 * (self.input_size // 4) * (self.input_size // 4))
		x = self.fc(x)

		return x


class CGAN:

	def __init__(self):

		# parameters
		self.epoch = 500
		self.batch_size = 64
		self.save_dir = 'ckpt/'
		self.result_dir = 'result/'
		self.dataset = 'db/mnist/'
		self.log_dir = 'result/'
		self.gpu_mode = True
		self.model_name = 'CGAN'
		self.input_size = 28
		self.z_dim = 62
		self.class_num = 10
		self.sample_num = self.class_num ** 2

		transform = transforms.Compose([transforms.Resize((self.input_size, self.input_size)), transforms.ToTensor(),
		                                transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))])

		self.data_loader = DataLoader( datasets.MNIST('data/mnist', train=True, download=True, transform=transform),
								batch_size=self.batch_size, shuffle=True)
		# [c, h, w]
		img0 = next(iter(self.data_loader))[0]

		# networks init
		self.G = Generator(input_dim=self.z_dim, output_dim=img0.shape[1], input_size=self.input_size,
		                   class_num=self.class_num)
		self.D = Discriminator(input_dim=img0.shape[1], output_dim=1, input_size=self.input_size,
		                       class_num=self.class_num)
		self.G_optimizer = optim.Adam(self.G.parameters(), lr=2e-4, betas=(0.5, 0.99))
		self.D_optimizer = optim.Adam(self.D.parameters(), lr=2e-4, betas=(0.5, 0.99))

		if self.gpu_mode:
			self.G.cuda()
			self.D.cuda()
			self.BCE_loss = nn.BCELoss().cuda()
		else:
			self.BCE_loss = nn.BCELoss()

		print(self.G, self.D)

		# fixed noise & condition
		# [100, 62]
		self.sample_z_ = torch.zeros((self.sample_num, self.z_dim))
		for i in range(self.class_num):
			self.sample_z_[i * self.class_num] = torch.rand(1, self.z_dim)
			for j in range(1, self.class_num):
				self.sample_z_[i * self.class_num + j] = self.sample_z_[i * self.class_num]

		temp = torch.zeros((self.class_num, 1))
		for i in range(self.class_num):
			temp[i, 0] = i

		temp_y = torch.zeros((self.sample_num, 1))
		for i in range(self.class_num):
			temp_y[i * self.class_num: (i + 1) * self.class_num] = temp

		self.sample_y_ = torch.zeros((self.sample_num, self.class_num)).scatter_(1, temp_y.type(torch.LongTensor), 1)
		if self.gpu_mode:
			self.sample_z_, self.sample_y_ = self.sample_z_.cuda(), self.sample_y_.cuda()

		self.vis = visdom.Visdom()





	def train(self):

		self.train_hist = dict()
		self.train_hist['D_loss'] = []
		self.train_hist['G_loss'] = []
		self.train_hist['per_epoch_time'] = []
		self.train_hist['total_time'] = []

		# [b, 1]
		self.y_real_, self.y_fake_ = torch.ones(self.batch_size, 1), torch.zeros(self.batch_size, 1)
		if self.gpu_mode:
			self.y_real_, self.y_fake_ = self.y_real_.cuda(), self.y_fake_.cuda()

		self.D.train()
		start_time = time.time()
		for epoch in range(self.epoch):

			self.G.train()
			epoch_start_time = time.time()

			for iter, (x_, y_) in enumerate(self.data_loader):
				if iter == self.data_loader.dataset.__len__() // self.batch_size:
					break

				# [b, z]
				z_ = torch.rand((self.batch_size, self.z_dim))
				# [b, 10]
				y_vec_ = torch.zeros((self.batch_size, self.class_num)).scatter_(1,
				                                                                 y_.type(torch.LongTensor).unsqueeze(1),
				                                                                 1)
				# [b, 10, 28, 28]
				y_fill_ = y_vec_.unsqueeze(2).unsqueeze(3).expand(self.batch_size, self.class_num, self.input_size,
				                                                  self.input_size)
				if self.gpu_mode:
					x_, z_, y_vec_, y_fill_ = x_.cuda(), z_.cuda(), y_vec_.cuda(), y_fill_.cuda()

				# update D network
				self.D_optimizer.zero_grad()
				D_real = self.D(x_, y_fill_)
				D_real_loss = self.BCE_loss(D_real, self.y_real_)

				G_ = self.G(z_, y_vec_)
				D_fake = self.D(G_, y_fill_)
				D_fake_loss = self.BCE_loss(D_fake, self.y_fake_)

				D_loss = D_real_loss + D_fake_loss
				self.train_hist['D_loss'].append(D_loss.item())

				D_loss.backward()
				self.D_optimizer.step()

				# update G network
				self.G_optimizer.zero_grad()

				G_ = self.G(z_, y_vec_)
				D_fake = self.D(G_, y_fill_)
				# force the generated image to deceit D
				G_loss = self.BCE_loss(D_fake, self.y_real_)
				self.train_hist['G_loss'].append(G_loss.item())

				G_loss.backward()
				self.G_optimizer.step()

				if ((iter + 1) % 100) == 0:
					print("Epoch: [%2d] [%4d/%4d] D_loss: %.8f, G_loss: %.8f" %
					      (
					      (epoch + 1), (iter + 1), self.data_loader.dataset.__len__() // self.batch_size, D_loss.item(),
					      G_loss.item()))

			self.train_hist['per_epoch_time'].append(time.time() - epoch_start_time)
			with torch.no_grad():
				self.visualize_results((epoch + 1))

		self.train_hist['total_time'].append(time.time() - start_time)
		print("Avg one epoch time: %.2f, total %d epochs time: %.2f" % (np.mean(self.train_hist['per_epoch_time']),
		                                                                self.epoch, self.train_hist['total_time'][0]))
		print("Training finish!... save training results")

		# self.save()
		# utils.generate_animation(self.result_dir + '/' + self.dataset + '/' + self.model_name + '/' + self.model_name,
		#                          self.epoch)
		# utils.loss_plot(self.train_hist, os.path.join(self.save_dir, self.dataset, self.model_name), self.model_name)

	def visualize_results(self, epoch, fix=True):
		self.G.eval()

		if not os.path.exists(self.result_dir + '/' + self.dataset + '/' + self.model_name):
			os.makedirs(self.result_dir + '/' + self.dataset + '/' + self.model_name)

		image_frame_dim = int(np.floor(np.sqrt(self.sample_num)))

		if fix:
			""" fixed noise """
			samples = self.G(self.sample_z_, self.sample_y_)
		else:
			""" random noise """
			sample_y_ = torch.zeros(self.batch_size, self.class_num).scatter_(1, torch.randint(0, self.class_num - 1, (
			self.batch_size, 1)).type(torch.LongTensor), 1)
			sample_z_ = torch.rand((self.batch_size, self.z_dim))
			if self.gpu_mode:
				sample_z_, sample_y_ = sample_z_.cuda(), sample_y_.cuda()

			samples = self.G(sample_z_, sample_y_)

		# if self.gpu_mode:
		# 	samples = samples.cpu().data.numpy()
		# else:
		# 	samples = samples.data.numpy()

		# de-normalize
		samples = (samples + 1) / 2
		samples = F.upsample(samples, scale_factor=2, mode='bilinear')
		# utils.save_images(samples[:image_frame_dim * image_frame_dim, :, :, :], [image_frame_dim, image_frame_dim],
		#                   self.result_dir + '/' + self.dataset + '/' + self.model_name + '/' + self.model_name + '_epoch%03d' % epoch + '.png')
		self.vis.images(samples, nrow=10, win='cgan')

	def save(self):
		save_dir = os.path.join(self.save_dir, self.dataset, self.model_name)

		if not os.path.exists(save_dir):
			os.makedirs(save_dir)

		torch.save(self.G.state_dict(), os.path.join(save_dir, self.model_name + '_G.pkl'))
		torch.save(self.D.state_dict(), os.path.join(save_dir, self.model_name + '_D.pkl'))

		with open(os.path.join(save_dir, self.model_name + '_history.pkl'), 'wb') as f:
			pickle.dump(self.train_hist, f)

	def load(self):
		save_dir = os.path.join(self.save_dir, self.dataset, self.model_name)

		self.G.load_state_dict(torch.load(os.path.join(save_dir, self.model_name + '_G.pkl')))
		self.D.load_state_dict(torch.load(os.path.join(save_dir, self.model_name + '_D.pkl')))




def main():
	gan = CGAN()

	gan.train()


if __name__ == '__main__':
	main()