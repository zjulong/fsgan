from PIL import Image
import os, errno
import os.path
import numpy as np
import torch
import pickle
import hashlib

import torch.utils.data as data
from    torchvision import transforms

def check_integrity(fpath, md5):
	if not os.path.isfile(fpath):
		return False
	md5o = hashlib.md5()
	with open(fpath, 'rb') as f:
		# read in 1MB chunks
		for chunk in iter(lambda: f.read(1024 * 1024), b''):
			md5o.update(chunk)
	md5c = md5o.hexdigest()
	if md5c != md5:
		return False
	return True


def download_url(url, root, filename, md5):
	from six.moves import urllib

	root = os.path.expanduser(root)
	fpath = os.path.join(root, filename)

	try:
		os.makedirs(root)
	except OSError as e:
		if e.errno == errno.EEXIST:
			pass
		else:
			raise

	# downloads file
	if os.path.isfile(fpath) and check_integrity(fpath, md5):
		print('Using downloaded and verified file: ' + fpath)
	else:
		try:
			print('Downloading ' + url + ' to ' + fpath)
			urllib.request.urlretrieve(url, fpath)
		except:
			if url[:5] == 'https':
				url = url.replace('https:', 'http:')
				print('Failed download. Trying https -> http instead.'
				      ' Downloading ' + url + ' to ' + fpath)
				urllib.request.urlretrieve(url, fpath)


class CIFAR10(data.Dataset):
	"""`CIFAR10 <https://www.cs.toronto.edu/~kriz/cifar.html>`_ Dataset.

	Args:
		root (string): Root directory of dataset where directory
			``cifar-10-batches-py`` exists or will be saved to if download is set to True.
		train (bool, optional): If True, creates dataset from training set, otherwise
			creates from test set.
		transform (callable, optional): A function/transform that  takes in an PIL image
			and returns a transformed version. E.g, ``transforms.RandomCrop``
		target_transform (callable, optional): A function/transform that takes in the
			target and transforms it.
		download (bool, optional): If true, downloads the dataset from the internet and
			puts it in root directory. If dataset is already downloaded, it is not
			downloaded again.

	"""
	base_folder = 'cifar-10-batches-py'
	url = "https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz"
	filename = "cifar-10-python.tar.gz"
	tgz_md5 = 'c58f30108f718f92721af3b95e74349a'
	train_list = [
		['data_batch_1', 'c99cafc152244af753f735de768cd75f'],
		['data_batch_2', 'd4bba439e000b95fd0a9bffe97cbabec'],
		['data_batch_3', '54ebc095f3ab1f0389bbae665268c751'],
		['data_batch_4', '634d18415352ddfa80567beed471001a'],
		['data_batch_5', '482c414d41f54cd18b22e5b47cb7c3cb'],
	]

	test_list = [
		['test_batch', '40351d587109b95175f43aff81a1287e'],
	]

	def __init__(self, root, batchsz, k, imgsz, total_num=10000, train=True):
		self.root = os.path.expanduser(root)
		self.train = train  # training set or test set
		self.batchsz = batchsz  # number of class
		self.k = k  # number of img per class
		self.imgsz = imgsz  # img size
		self.total_num = total_num  # total batch number to end

		self.download()

		if not self._check_integrity():
			raise RuntimeError('Dataset not found or corrupted.' +
			                   ' You can use download=True to download it')

		# now load the picked numpy arrays
		# 1.load train data
		train_data = []
		train_labels = []
		for fentry in self.train_list:
			f = fentry[0]
			file = os.path.join(self.root, self.base_folder, f)
			fo = open(file, 'rb')
			entry = pickle.load(fo, encoding='latin1')
			train_data.append(entry['data'])
			if 'labels' in entry:
				train_labels += entry['labels']
			else:
				train_labels += entry['fine_labels']
			fo.close()

		train_data = np.concatenate(train_data)
		train_data = train_data.reshape((50000, 3, 32, 32))
		train_data = train_data.transpose((0, 2, 3, 1))  # convert to HWC

		# 2.load test data
		f = self.test_list[0][0]
		file = os.path.join(self.root, self.base_folder, f)
		fo = open(file, 'rb')
		entry = pickle.load(fo, encoding='latin1')
		test_data = entry['data']
		if 'labels' in entry:
			test_labels = entry['labels']
		else:
			test_labels = entry['fine_labels']
		fo.close()

		test_data = test_data.reshape((10000, 3, 32, 32))
		test_data = test_data.transpose((0, 2, 3, 1))  # convert to HWC, for PIL read

		# (50000, 32, 32, 3), (10000, 32, 32, 3)
		# print(train_data.shape, train_labels)
		# print(test_data.shape, test_labels)
		# print(train_labels)
		# print(test_labels)

		data = np.concatenate([train_data, test_data], axis=0)
		labels_ = np.concatenate([train_labels, test_labels], axis=0)
		# (60000, 32, 32, 3) (60000,)
		# print(data.shape, labels.shape)
		labels = np.sort(labels_)
		# ERROR! labels have been sorted!!!
		# indices = np.argsort(labels)
		indices = np.argsort(labels_)
		data = data[indices]

		# 0000 0 => 6000 1=>12000 2=>18000 3=>24000 4=>30000 5=>36000 6=>42000 7=>48000 8=>54000 9=>60000
		data2 = [
			data[0    : 6000],
			data[6000 :12000],
			data[12000:18000],
			data[18000:24000],
			data[24000:30000],
			data[30000:36000],
			data[36000:42000],
			data[42000:48000],
			data[48000:54000],
			data[54000:60000]
		]
		labels2 = [
			labels[0    : 6000],
			labels[6000 :12000],
			labels[12000:18000],
			labels[18000:24000],
			labels[24000:30000],
			labels[30000:36000],
			labels[36000:42000],
			labels[42000:48000],
			labels[48000:54000],
			labels[54000:60000]
		]
		if train:
			self.indices = indices = [0, 1, 2, 3, 4, 5, 6, 7]
		else:
			self.indices = indices = [8, 9]

		self.data, self.labels = data2, labels2
		self.train_data, self.train_labels = train_data, train_labels
		self.test_data, self.test_labels = test_data, test_labels
		print('CIFAR10 train:', self.train, 'indices:', indices, 'b:', batchsz, 'k:', k, 'imgsz:', imgsz, 'num:', total_num)

	def __getitem__(self, index):
		"""
		Args:
			index (int): Index

		Returns:
			tuple: (image, target) where target is index of the target class.
		"""
		# NO duplicate class allowed
		# NOTICE: self.indices only contains trian set or test set.
		select_class = np.random.choice(self.indices, self.batchsz, replace=False)
		img, target = [], []
		for i in select_class:
			# allow duplicate imgs in class
			select_img = np.random.choice(len(self.labels[i]), self.k, replace=True)

			for j in select_img:
				img.append(self.data[i][j])
				target.append(self.labels[i][j])

		# [b*k, 64, 64, 3]
		# [b*k]
		img, target = np.stack(img, axis=0), np.stack(target, axis=0)
		# [32, 64, 64, 3] [32]
		# print(img.shape, target.shape)

		trans = transforms.Compose([
			lambda x:Image.fromarray(x, mode='RGB'),
			transforms.Resize((self.imgsz, self.imgsz)),
			transforms.ToTensor(),
			transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])

		])
		# [b*k, 64, 64, 3] => [b*k, 3, 64, 64]
		img = list(map(lambda x:trans(x), img))
		img = torch.stack(img)
		target = torch.from_numpy(target)

		# [32, 3, 64, 64] => [b, k, 3, 64, 64]
		# [32] => [b, k]
		img, target = img.view(self.batchsz, self.k, 3, self.imgsz, self.imgsz), target.view(self.batchsz, self.k)
		img = img
		# print(img.shape, target.shape)
		# print(target)

		return img, target









	def __len__(self):
		return self.total_num

	def _check_integrity(self):
		root = self.root
		for fentry in (self.train_list + self.test_list):
			filename, md5 = fentry[0], fentry[1]
			fpath = os.path.join(root, self.base_folder, filename)
			if not check_integrity(fpath, md5):
				return False
		return True

	def download(self):
		import tarfile

		if self._check_integrity():
			print('Files already downloaded and verified')
			return

		root = self.root
		download_url(self.url, root, self.filename, self.tgz_md5)

		# extract file
		cwd = os.getcwd()
		tar = tarfile.open(os.path.join(root, self.filename), "r:gz")
		os.chdir(root)
		tar.extractall()
		tar.close()
		os.chdir(cwd)

	def __repr__(self):
		fmt_str = 'Dataset ' + self.__class__.__name__ + '\n'
		fmt_str += '    Number of datapoints: {}\n'.format(self.__len__())
		tmp = 'train' if self.train is True else 'test'
		fmt_str += '    Split: {}\n'.format(tmp)
		fmt_str += '    Root Location: {}\n'.format(self.root)
		tmp = '    Transforms (if any): '
		fmt_str += '{0}{1}\n'.format(tmp, self.transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
		tmp = '    Target Transforms (if any): '
		fmt_str += '{0}{1}'.format(tmp, self.target_transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
		return fmt_str


class CIFAR100(CIFAR10):
	"""`CIFAR100 <https://www.cs.toronto.edu/~kriz/cifar.html>`_ Dataset.

	This is a subclass of the `CIFAR10` Dataset.
	"""
	base_folder = 'cifar-100-python'
	url = "https://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz"
	filename = "cifar-100-python.tar.gz"
	tgz_md5 = 'eb9058c3a382ffc7106e4002c42a8d85'
	train_list = [
		['train', '16019d7e3df5f24257cddd939b257f8d'],
	]

	test_list = [
		['test', 'f0ef6b0ae62326f3e7ffdfab6717acfc'],
	]


def main():
	import visdom
	import time

	cifar = CIFAR10('db/', batchsz=2, k=8, imgsz=64, train=True)

	vis = visdom.Visdom()

	for x, label in cifar:


		x = x.view(x.size(0) * x.size(1), 3, 64, 64)
		x = 0.5 * x + 0.5
		vis.images(x, nrow=8, win='cifar10-db')
		vis.text(str(label.numpy()), win='cifar10-db-label')

		time.sleep(2)




if __name__ == '__main__':
	main()
