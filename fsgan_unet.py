import torch
from    torch import nn
from    torch import optim
import  numpy as np


class Flatten(nn.Module):
	def __init__(self):
		super(Flatten, self).__init__()

	def forward(self, input):
		"""

		:param input: [b, dimx,...]
		:return: [b, -1]
		"""
		# print('flatten:', input.shape)
		return input.view(input.size(0), -1)


def init_weights(net, init_type='normal', gain=0.02):
	def init_func(m):
		classname = m.__class__.__name__
		if hasattr(m, 'weight') and (classname.find('Conv') != -1 or classname.find('Linear') != -1):
			if init_type == 'normal':
				nn.init.normal_(m.weight.data, 0.0, gain)
			elif init_type == 'xavier':
				nn.init.xavier_normal_(m.weight.data, gain=gain)
			elif init_type == 'kaiming':
				nn.init.kaiming_normal_(m.weight.data, a=0, mode='fan_in')
			elif init_type == 'orthogonal':
				nn.init.orthogonal_(m.weight.data, gain=gain)
			else:
				raise NotImplementedError('initialization method [%s] is not implemented' % init_type)
			if hasattr(m, 'bias') and m.bias is not None:
				nn.init.constant_(m.bias.data, 0.0)
		elif classname.find('BatchNorm2d') != -1:
			nn.init.normal_(m.weight.data, 1.0, gain)
			nn.init.constant_(m.bias.data, 0.0)

	print('Initialize network with %s.' % init_type)
	net.apply(init_func)


class UnetSkipBlock(nn.Module):
	"""
	Outmost, middle, innermost unet block.
	"""

	def __init__(self, outer_nc, inner_nc, input_nc=None,
	             submodule=None, outermost=False, innermost=False, name='unet-block', use_dropout=False):
		"""

		:param outer_nc: output channel
		:param inner_nc: inner channel
		:param input_nc: input channel
		:param submodule: nested unet block reference
		:param outermost: is this block the top level
		:param innermost: is this block the bottom level
		:param name: name, for debug printing
		:param use_dropout:
		"""
		super(UnetSkipBlock, self).__init__()

		self.outermost = outermost
		self.name = name

		norm_layer = nn.BatchNorm2d
		use_bias = (norm_layer == nn.InstanceNorm2d)

		if input_nc is None:
			input_nc = outer_nc

		downconv = nn.Conv2d(input_nc, inner_nc, kernel_size=4, stride=2, padding=1, bias=use_bias)
		downrelu = nn.LeakyReLU(0.2, True)
		downnorm = norm_layer(inner_nc)
		uprelu = nn.ReLU(True)
		upnorm = norm_layer(outer_nc)

		# if this is the Top-level unet block
		if outermost:
			upconv = nn.ConvTranspose2d(inner_nc * 2, outer_nc, kernel_size=4, stride=2, padding=1)
			down = [downconv]
			up = [uprelu, upconv, nn.Tanh()]
			model = down + [submodule] + up
		# if this is the Bottom-level unet block
		elif innermost:
			upconv = nn.ConvTranspose2d(inner_nc, outer_nc, kernel_size=4, stride=2, padding=1, bias=use_bias)
			down = [downrelu, downconv]
			up = [uprelu, upconv, upnorm]
			model = down + up
		# if this is the middle level unet block
		else:
			upconv = nn.ConvTranspose2d(inner_nc * 2, outer_nc, kernel_size=4, stride=2, padding=1, bias=use_bias)
			down = [downrelu, downconv, downnorm]
			up = [uprelu, upconv, upnorm]

			if use_dropout:
				model = down + [submodule] + up + [nn.Dropout(0.5)]
			else:
				model = down + [submodule] + up

		self.model = nn.Sequential(*model)

	def forward(self, x):
		"""

		:param x: [b, c, h, w]
		:return:
		"""
		# print(self.name, x.shape)
		if self.outermost:
			x = self.model(x)
		else:
			# concat along c dim
			x_ = self.model(x)
			x = torch.cat([x, x_], dim=1)
		# print(self.name, 'cat:', x_.shape)
		# print(self.name, 'out:', x.shape)

		return x


class UnetGenerator(nn.Module):
	"""

	Defines the Unet generator.
	"""

	def __init__(self, inpc=1, factor=64, use_dropout=True):
		"""

		:param inpc: input image channel, = output image channel
		:param factor:
		:param use_dropout:
		"""
		super(UnetGenerator, self).__init__()

		# 1. build the inner-most unet block
		unet_block = UnetSkipBlock(factor * 8, factor * 8, input_nc=None, submodule=None, innermost=True, name='    bound')

		# 2. build 2-4 middle unet block
		# unet_block = UnetSkipBlock(factor * 8, factor * 8, input_nc=None, submodule=unet_block, use_dropout=use_dropout,
		#                            name='     middle-6')
		# unet_block = UnetSkipBlock(factor * 8, factor * 8, input_nc=None, submodule=unet_block, use_dropout=use_dropout,
		#                            name='    middle-5')
		# unet_block = UnetSkipBlock(factor * 8, factor * 8, input_nc=None, submodule=unet_block, use_dropout=use_dropout,
		#                            name='    middle-4')

		# 3. build other 5-7 middle unet block
		unet_block = UnetSkipBlock(factor * 4, factor * 8, input_nc=None, submodule=unet_block,
		                           name='   middle-3', use_dropout=use_dropout)
		unet_block = UnetSkipBlock(factor * 2, factor * 4, input_nc=None, submodule=unet_block,
		                           name='  middle-2', use_dropout=use_dropout)
		unet_block = UnetSkipBlock(factor, factor * 2, input_nc=None, submodule=unet_block,
		                           name=' middle-1', use_dropout=use_dropout)
		# 4. build the outer-most unet block
		self.model = UnetSkipBlock(inpc, factor, input_nc=inpc, submodule=unet_block, outermost=True, name='top')

	def forward(self, x):
		"""

		:param x: [b, c, h, w]
		:return:
		"""
		return self.model(x)




def set_grad(nets, requires_grad=False):
	if not isinstance(nets, list):
		nets = [nets]
	for net in nets:
		if net is not None:
			for param in net.parameters():
				param.requires_grad = requires_grad


class FSGAN(nn.Module):
	def __init__(self):
		super(FSGAN, self).__init__()
		factor = 8

		# [b, 1, 64, 64] => [b, 1, 64, 64]
		# TODO: add latent code
		# self.generator = UnetGenerator(factor=factor, use_dropout=False)

		# 2x[b, 1, 64, 64] => [b, 1, 64, 64]
		self.generator = nn.Sequential(
			nn.ConvTranspose2d(1, factor * 8, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(factor * 8),
			# nn.Dropout(),
			nn.ReLU(inplace=True),

			nn.ConvTranspose2d(factor * 8, factor * 4, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(factor * 4),
			# nn.Dropout(),
			nn.ReLU(inplace=True),

			nn.ConvTranspose2d(factor * 4, factor * 2, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(factor * 2),
			# nn.Dropout(),
			nn.ReLU(inplace=True),

			nn.ConvTranspose2d(factor * 2, factor * 1, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(factor * 1),
			nn.ReLU(inplace=True),

			nn.ConvTranspose2d(factor * 1, 1, kernel_size=3, stride=1, padding=1),
			# no batchnorm by DCGAN
			nn.Tanh(),
		)


		# [b, 1, 64, 64] => [b, 128, 11, 11]
		# 32: 2 conv units: [b, 1, 32, 32] => [b, 32, 7, 7]
		self.disc_rep = nn.Sequential(
			nn.Conv2d(1, factor * 1, kernel_size=3, stride=2),
			# no batchnorm by DCGAN
			nn.LeakyReLU(inplace=True, negative_slope=0.2),

			nn.Conv2d(factor * 1, factor * 2, kernel_size=3, stride=2),
			nn.BatchNorm2d(factor * 2),
			nn.LeakyReLU(inplace=True, negative_slope=0.2),

			nn.Conv2d(factor * 2, factor * 4, kernel_size=3, stride=1),
			nn.BatchNorm2d(factor * 4),
			nn.LeakyReLU(inplace=True, negative_slope=0.2),
			#
			# nn.Conv2d(factor * 4, factor * 8, kernel_size=3, stride=1),
			# nn.BatchNorm2d(factor * 8),
			# nn.LeakyReLU(inplace=True)
		)

		# feature map concatenate: 2 x [b, 128, 11, 11] => [b, 256, 11, 11]
		# for 32 size: 2x[b, 16, 7, 7] => [b, 32, 7, 7]
		self.disc_cmp = nn.Sequential(
			nn.Conv2d(factor * 4 * 2, 32, kernel_size=3, stride=1),
			nn.BatchNorm2d(32),
			nn.ReLU(inplace=True),

			nn.Conv2d(32, 16, kernel_size=3, stride=1),
			nn.BatchNorm2d(16),
			nn.ReLU(inplace=True),

			# [b, 32, 1, 1]
			Flatten(),
			nn.Linear(16, 1),
			# nn.ReLU(inplace=True),
			# nn.Linear(16, 1),
			nn.Sigmoid()

		)

		# simply to indicate the current device context, ()
		self.non_tensor = nn.Parameter(torch.tensor(0))

		self.g_optim = optim.Adam(self.generator.parameters(), lr=2e-4, betas=(0.5, 0.999))
		self.d_optim = optim.Adam(list(self.disc_rep.parameters()) + list(self.disc_cmp.parameters()),
		                          lr=2e-4, betas=(0.5, 0.999))
		self.criteon = nn.BCELoss(size_average=True)
		self.l1criteon = nn.L1Loss()

		# init
		# init_weights(self.generator)
		# init_weights(self.disc_rep)
		# init_weights(self.disc_cmp)

	def forward(self, x, z, y):
		"""

		:param x: [b, k, 1, h, w]
		:param z: [b, k, 1, h, w]
		:param y: [b, k]
		:return:
		"""
		batchsz, k, c_, h, w = x.size()
		assert h == w

		# 1. train D firstly
		# [b, k, 2, h, w] => [b*k, 2, h, w] => [b*k, 1, h, w] => [b, k, 1, h, w]
		x_hat = self.generator(x.view(batchsz * k, c_, h, h)).view(batchsz, k, c_, h, h)

		# [b, k, 1, h, w] => [1, b, k, 1, h, w] => [1, b, 1, k, 1, h, w] => [b, b, k, k, 1, h, w]
		x1 = x.unsqueeze(0).unsqueeze(2).expand(batchsz, batchsz, k, k, c_, h, h)
		# [b, k, 1, h, w] => [b, 1, k, 1, h, w] => [b, 1, k, 1, 1, h, w] => [b, b, k, k, 1, h, w]
		x2 = x.unsqueeze(1).unsqueeze(3).expand(batchsz, batchsz, k, k, c_, h, h)
		# [b, b, k, k, 1, h, w] => [b, b, k, k, 2, h, w] => [new_b, imgs]
		x_all = torch.cat([x1, x2], dim=4).view(batchsz * batchsz * k * k, 2 * c_, h, w)

		# [b, k] => [b, k, 1]
		y_ = y.unsqueeze(2)
		# [b, k, 1] => [1, b, k, 1] => [1, b, 1, k, 1] => [b, b, k, k, 1]
		y1 = y_.unsqueeze(0).unsqueeze(2).expand(batchsz, batchsz, k, k, 1)
		# [b, k, 1] => [b, 1, k, 1] => [b, 1, k, 1, 1] => [b, b, k, k, 1]
		y2 = y_.unsqueeze(1).unsqueeze(3).expand(batchsz, batchsz, k, k, 1)
		# eq [b, b, k, k, 1] => byte [b, b, k, k, 1] => long [new_b, 1]
		label_all = torch.eq(y1, y2).float().view(batchsz * batchsz * k * k, 1)

		# [b, k, 1]
		y_ = torch.from_numpy(np.arange(batchsz * k).reshape(batchsz, k, 1))
		y_ = y_.to(self.non_tensor.device)
		# [b, k, 1] => [1, b, k, 1] => [1, b, 1, k, 1] => [b, b, k, k, 1]
		y1 = y_.unsqueeze(0).unsqueeze(2).expand(batchsz, batchsz, k, k, 1)
		# [b, k, 1] => [b, 1, k, 1] => [b, 1, k, 1, 1] => [b, b, k, k, 1]
		y2 = y_.unsqueeze(1).unsqueeze(3).expand(batchsz, batchsz, k, k, 1)
		# eq [b, b, k, k, 1] => byte [b, b, k, k, 1] => long [new_b, 1]
		label_all_same = torch.eq(y1, y2).view(batchsz * batchsz * k * k, 1)
		# set the same image pairs to 0 similarity
		# print('be:', label_all)
		# print('same', label_all_same)
		label_all = torch.where(label_all_same, torch.zeros(batchsz * batchsz * k * k, 1).to(self.non_tensor.device),
		                                        label_all)
		# print('af:', label_all)


		# [b, k, 1, h, w] => [b, 1, k, 1, h, w] => [b, k, k, 1, h, w]
		x_n1 = x.unsqueeze(1).expand(batchsz, k, k, c_, h, h)
		# [b, k, 1, h, w] => [b, k, 1, 1, h, w] => [b, k, k, 1, h, w]
		x_n2 = x_hat.unsqueeze(2).expand(batchsz, k, k, c_, h, h)
		# [b, k, k, 2, h, w] => [new_b, imgs]
		x_n_fake = torch.cat([x_n1, x_n2], dim=3).view(batchsz * k * k, 2 * c_, h, h)
		# fake label for x_n_fake: [new_b, 1]
		label_n_fake = torch.zeros(batchsz * k * k, 1).to(self.non_tensor.device)

		# merge all data
		# [new_b, 2, h, w] with [new_b, 2, h, w] => [new_b, 2, h, w]
		x_all = torch.cat([x_all, x_n_fake], dim=0)
		# [new_b, 1] with [new_b, 1] => [new_b, 1] => [new_b]
		label_all = torch.cat([label_all, label_n_fake], dim=0).squeeze(1)

		# update batchsz <= new_b
		batchsz = x_all.size(0)
		# our Discriminator network is distinct from conventional D
		# x_all: [b, 2, h, w] => [b, 1, h, w] & [b, 1, h, w]
		x1, x2 = torch.chunk(x_all, 2, dim=1)
		# get feature maps of image pairs
		# [b, 1, 64, 64] => [b, 512, 9, 9]
		x1, x2 = self.disc_rep(x1), self.disc_rep(x2)
		# print(x1.shape)
		# concatenate x1 and x2
		# [b, 512, 9, 9] => [b, 1024, 9, 9]
		comb = torch.cat([x1, x2], dim=1)
		# get similarity scores
		# [b, 1024, 9, 9] => [b, 1] => [b]
		score = self.disc_cmp(comb).squeeze(1)
		# get loss of D
		# [b] vs [b]
		loss_d = self.criteon(score, label_all) * 0.5
		# backprop
		self.d_optim.zero_grad()
		# set_grad([self.disc_rep, self.disc_cmp], requires_grad=True)
		loss_d.backward()
		# print('>>d:')
		# for p in self.disc_rep.parameters():
		# 	print(p.norm())
		# for p in self.disc_cmp.parameters():
		# 	print(p.norm())
		self.d_optim.step()

		# 2. train G
		batchsz = x.size(0)
		# [b, k, 2, h, w] => [b*k, 2, h, w] => [b*k, 1, h, w] => [b, k, 1, h, w]
		x_hat = self.generator(x.view(batchsz * k, c_, h, h)).view(batchsz, k, c_, h, h)
		# [b, k, 1, h, w] => [b, 1, k, 1, h, w] => [b, k, k, 1, h, w]
		x_n1 = x.unsqueeze(1).expand(batchsz, k, k, c_, h, h)
		# [b, k, 1, h, w] => [b, k, 1, 1, h, w] => [b, k, k, 1, h, w]
		x_n2 = x_hat.unsqueeze(2).expand(batchsz, k, k, c_, h, h)
		# [b, k, k, 2, h, w] => [new_b, imgs]
		x_n_fake = torch.cat([x_n1, x_n2], dim=3).view(batchsz * k * k, 2 * c_, h, h)

		# update batchsz <= new_b
		batchsz = x_n_fake.size(0)
		# x_n_fake: [b, 2, h, w] => [b, 1, h, w]
		x1, x2 = torch.chunk(x_n_fake, 2, dim=1)
		# NOTICE: to make G improve, we need treat it as real image
		label = torch.ones(batchsz).to(self.non_tensor.device)
		# get features maps
		# [b, 512, 9, 9]
		x1, x2 = self.disc_rep(x1), self.disc_rep(x2)
		# concatenate x1 and x2
		# [b, 512, 9, 9] => [b, 1024, 9, 9]
		comb = torch.cat([x1, x2], dim=1)
		# get similarity scores
		# [b, 1024, 9, 9] => [b]
		score = self.disc_cmp(comb).squeeze(1)
		# compute loss of G
		loss_g = self.criteon(score, label) # + self.l1criteon(x_hat, x)
		loss_g = loss_g * 100
		# backprop
		self.g_optim.zero_grad()
		# set_grad([self.disc_rep, self.disc_cmp], requires_grad=False)
		loss_g.backward()
		# print('>>G:')
		# for p in self.generator.parameters():
		# 	print(p.norm())
		# nn.utils.clip_grad_norm_(self.generator.parameters(), 10)
		self.g_optim.step()

		return loss_d, loss_g, x_hat

	def generate(self, x, z):
		"""

		:param x:
		:param z:
		:return:
		"""
		batchsz, k, c_, h, w = x.size()

		# 1. train D firstly
		# [b, k, 2, h, w] => [b*k, 2, h, w] => [b*k, 1, h, w] => [b, k, 1, h, w]
		x_hat = self.generator(x.view(batchsz * k, c_, h, h)).view(batchsz, k, c_, h, h)

		return x_hat


def main():
	c_ = 1
	gan = FSGAN()

	x = torch.randn(4, c_, 64, 64)

	out1 = gan.disc_rep(x)
	out2 = gan.disc_rep(x)
	print('rep:', out1.shape)
	out = torch.cat([out1, out2], dim=1)

	out = gan.disc_cmp(out)
	print('cmp:', out.shape)

	x = torch.randn(4, c_, 64, 64)
	out = gan.generator(x)
	print('g:', out.shape)


if __name__ == '__main__':
	main()
