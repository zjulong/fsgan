import  torch
from    torch.utils.data import Dataset
import  os
from    PIL import Image
import  errno
import  numpy as np
import  codecs
from    torchvision import transforms

def get_int(b):
	return int(codecs.encode(b, 'hex'), 16)


def read_label_file(path):
	with open(path, 'rb') as f:
		data = f.read()
		assert get_int(data[:4]) == 2049
		length = get_int(data[4:8])
		parsed = np.frombuffer(data, dtype=np.uint8, offset=8)
		return torch.from_numpy(parsed).view(length).long()


def read_image_file(path):
	with open(path, 'rb') as f:
		data = f.read()
		assert get_int(data[:4]) == 2051
		length = get_int(data[4:8])
		num_rows = get_int(data[8:12])
		num_cols = get_int(data[12:16])
		images = []
		parsed = np.frombuffer(data, dtype=np.uint8, offset=16)
		return torch.from_numpy(parsed).view(length, num_rows, num_cols)


class MNIST(Dataset):
	"""`MNIST <http://yann.lecun.com/exdb/mnist/>`_ Dataset.

	Args:
		root (string): Root directory of dataset where ``processed/training.pt``
			and  ``processed/test.pt`` exist.
		train (bool, optional): If True, creates dataset from ``training.pt``,
			otherwise from ``test.pt``.
		download (bool, optional): If true, downloads the dataset from the internet and
			puts it in root directory. If dataset is already downloaded, it is not
			downloaded again.
		transform (callable, optional): A function/transform that  takes in an PIL image
			and returns a transformed version. E.g, ``transforms.RandomCrop``
		target_transform (callable, optional): A function/transform that takes in the
			target and transforms it.
	"""
	urls = [
		'http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz',
		'http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz',
		'http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz',
		'http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz',
	]
	raw_folder = 'raw'
	processed_folder = 'processed'
	training_file = 'training.pt'
	test_file = 'test.pt'

	def __init__(self, root, batchsz, k, imgsz, total_num=10000, train=True):
		self.root = os.path.expanduser(root)
		self.train = train  # training set or test set
		self.batchsz = batchsz # number of class
		self.k = k  # number of img per class
		self.imgsz = imgsz # img size
		self.total_num = total_num # total batch number to end

		self.download()

		if not self._check_exists():
			raise RuntimeError('Dataset not found.' +
			                   ' You can use download=True to download it')

		train_data, train_labels = torch.load(
			os.path.join(self.root, self.processed_folder, self.training_file))
		test_data, test_labels = torch.load(
			os.path.join(self.root, self.processed_folder, self.test_file))

		# torch.Size([60000, 28, 28])
		# torch.Size([60000])
		# torch.Size([10000, 28, 28])
		# torch.Size([10000])
		# print(train_data.shape, train_labels.shape)
		# print(test_data.shape, test_labels.shape)
		data = torch.cat([train_data, test_data], dim=0)
		labels = torch.cat([train_labels, test_labels], dim=0)
		print(data.shape, labels.shape)
		labels, indices = torch.sort(labels)
		data = data[indices]

		# 0: 0-6702
		# 1: 6703-14779
		#[0-6903-14780-21770-28911-35735-42048-48924-56217-63042-69999]
		data = [
			data[0    : 6903],
			data[6903 :14780],
			data[14780:21770],
			data[21770:28911],
			data[28911:35735],
			data[35735:42048],
			data[42048:48924],
			data[48924:56217],
			data[56217:63042],
			data[63042:70000]
		]
		labels = [
			labels[0    : 6903],
			labels[6903 :14780],
			labels[14780:21770],
			labels[21770:28911],
			labels[28911:35735],
			labels[35735:42048],
			labels[42048:48924],
			labels[48924:56217],
			labels[56217:63042],
			labels[63042:70000]
		]
		if train:
			self.indices = indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
		else:
			self.indices = indices = [8, 9]

		self.data, self.labels = data, labels
		print('MNIST train:', self.train, 'indices:', indices, 'b:', batchsz, 'k:', k, 'imgsz:', imgsz, 'num:', total_num)


	def __getitem__(self, index):
		"""
		Args:
			index (int): Index

		Returns:
			tuple: (image, target) where target is index of the target class.
		"""
		# NO duplicate class allowed
		select_class = np.random.choice(self.indices, self.batchsz, replace=False)
		img, target = [], []
		for i in select_class:
			# allow duplicate imgs in class
			select_img = np.random.choice(len(self.labels[i]), self.k, replace=True)


			for j in select_img:
				img.append(self.data[i][j])
				target.append(self.labels[i][j])

		# [b*k, 28, 28]
		# [b*k]
		img, target = torch.stack(img), torch.stack(target)
		# torch.Size([32, 28, 28]) torch.Size([32])
		# print(img.shape, target.shape)

		trans = transforms.Compose([
			lambda x:Image.fromarray(x.numpy(), mode='L'),
			transforms.Resize((self.imgsz, self.imgsz)),
			transforms.ToTensor()

		])
		img = list(map(lambda x:trans(x), img))
		img = torch.stack(img)

		# [32, 1, 64, 64] => [b, k, 1, 64, 64]
		# [32] => [b, k]
		img, target = img.view(self.batchsz, self.k, 1, self.imgsz, self.imgsz), target.view(self.batchsz, self.k)
		# print(img.shape, target.shape)
		# print(target)

		return img, target


	def __len__(self):
		return self.total_num


	def _check_exists(self):
		return os.path.exists(os.path.join(self.root, self.processed_folder, self.training_file)) and \
		       os.path.exists(os.path.join(self.root, self.processed_folder, self.test_file))


	def download(self):
		"""Download the MNIST data if it doesn't exist in processed_folder already."""
		from six.moves import urllib
		import gzip

		if self._check_exists():
			return

		# download files
		try:
			os.makedirs(os.path.join(self.root, self.raw_folder))
			os.makedirs(os.path.join(self.root, self.processed_folder))
		except OSError as e:
			if e.errno == errno.EEXIST:
				pass
			else:
				raise

		for url in self.urls:
			print('Downloading ' + url)
			data = urllib.request.urlopen(url)
			filename = url.rpartition('/')[2]
			file_path = os.path.join(self.root, self.raw_folder, filename)
			with open(file_path, 'wb') as f:
				f.write(data.read())
			with open(file_path.replace('.gz', ''), 'wb') as out_f, \
					gzip.GzipFile(file_path) as zip_f:
				out_f.write(zip_f.read())
			os.unlink(file_path)

		# process and save as torch files
		print('Processing...')

		training_set = (
			read_image_file(os.path.join(self.root, self.raw_folder, 'train-images-idx3-ubyte')),
			read_label_file(os.path.join(self.root, self.raw_folder, 'train-labels-idx1-ubyte'))
		)
		test_set = (
			read_image_file(os.path.join(self.root, self.raw_folder, 't10k-images-idx3-ubyte')),
			read_label_file(os.path.join(self.root, self.raw_folder, 't10k-labels-idx1-ubyte'))
		)
		with open(os.path.join(self.root, self.processed_folder, self.training_file), 'wb') as f:
			torch.save(training_set, f)
		with open(os.path.join(self.root, self.processed_folder, self.test_file), 'wb') as f:
			torch.save(test_set, f)

		print('Done!')


def main():
	import visdom
	import time

	cifar = MNIST('db/mnist', batchsz=2, k=8, imgsz=64, train=True)

	vis = visdom.Visdom()

	for x, label in cifar:


		x = x.view(x.size(0) * x.size(1), 1, 64, 64)
		# x = 0.5 * x + 0.5
		vis.images(x, nrow=8, win='mnist-db')
		vis.text(str(label.numpy()), win='mnist-db-label')

		time.sleep(2)


if __name__ == '__main__':
	main()
