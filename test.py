
import torch







def main():
	b = 2
	k = 3
	# [b, k, 1]
	x = torch.tensor([
						[[1.1], [1.2], [1.3]],
	                    [[2.1], [2.2], [2.3]]
	                  ])
	label = x.long()
	x_hat = torch.tensor([
						[[1.1001], [1.2002], [1.3003]],
	                    [[2.1001], [2.2002], [2.3003]]
	                  ])


	# [b, k, 1] => [1, b, 1, k, 1] => [b, b, k, k, 1]
	x1 = x.unsqueeze(0).unsqueeze(2).expand(b, b, k, k, 1)
	# [b, k, 1] => [b, 1, k, 1] => [b, 1, k, 1, 1] => [b, b, k, k, 1]
	x2 = x.unsqueeze(1).unsqueeze(3).expand(b, b, k, k, 1)
	# [b, b, k, k, 1] => [b, b, k, k, 2]
	x_all = torch.cat([x1, x2], dim=4)

	# [b, k, 1]
	y = label
	# [b, k, 1] => [1, b, 1, k, 1] => [b, b, k, k, 1]
	y1 = y.unsqueeze(0).unsqueeze(2).expand(b, b, k, k, 1)
	# [b, k, 1] => [b, 1, k, 1] => [b, 1, k, 1, 1] => [b, b, k, k, 1]
	y2 = y.unsqueeze(1).unsqueeze(3).expand(b, b, k, k, 1)
	# [b, b, k, k, 1] eq [b, b, k, k, 1]
	y = torch.eq(y1, y2)

	# [b, k, 1] => [b, 1, k, 1] => [b, k, k, 1]
	x_n1 = x.unsqueeze(1).expand(b, k, k, 1)
	# [b, k, 1] => [b, k, 1, 1] => [b, k, k, 1]
	x_n2 = x_hat.unsqueeze(2).expand(b, k, k, 1)
	# [b, k, k, 1] => [b, k, k, 2]
	x_n_fake = torch.cat([x_n1, x_n2], dim=3)

	print('in class all:')
	for bi in range(b):
		for bj in range(b):
			for ki in range(k):
				for kj in range(k):
					print(bi, bj, ki, kj, x_all[bi, bj, ki, kj], y[bi, bj, ki, kj])

	print('real-fake negative pairs')
	for bi in range(b):
		for ki in range(k * k):
			print(bi, ki, x_n_fake[bi, ki // k, ki % k])






if __name__ == '__main__':
	main()