import  torch, math
import  numpy as np
import  torch.nn as nn
import  torch.optim as optim
from    torch.utils.data import DataLoader, TensorDataset
from    torchvision import datasets, transforms
import  visdom
from    torch.nn import functional as F
import  gc







class Generator:

	def __init__(self, z_dim, c_dim, device):

		# according to Pytorch w/b format, w = [out_dim, in_dim]
		# b = [out_dim]
		self.vars = [
			# [z+c, 128]
			torch.ones(128, z_dim + c_dim, requires_grad=True, device=device),
			torch.zeros(128, requires_grad=True, device=device),
			torch.rand(128, requires_grad=True, device=device),
			torch.zeros(128, requires_grad=True, device=device),
			# [128, 256]
			torch.ones(256, 128, requires_grad=True, device=device),
			torch.zeros(256, requires_grad=True, device=device),
			torch.rand(256, requires_grad=True, device=device),
			torch.zeros(256, requires_grad=True, device=device),
			# [256, 512]
			torch.ones(512, 256, requires_grad=True, device=device),
			torch.zeros(512, requires_grad=True, device=device),
			torch.rand(512, requires_grad=True, device=device),
			torch.zeros(512, requires_grad=True, device=device),
			# [512, 1024]
			torch.ones(1024, 512, requires_grad=True, device=device),
			torch.zeros(1024, requires_grad=True, device=device),
			torch.rand(1024, requires_grad=True, device=device),
			torch.zeros(1024, requires_grad=True, device=device),
			# [1024, 28*28]
			torch.ones(28 * 28, 1024, requires_grad=True, device=device),
			torch.zeros(28 * 28, requires_grad=True, device=device),
			torch.rand(28 * 28, requires_grad=True, device=device),
			torch.zeros(28 * 28, requires_grad=True, device=device),
		]

		# moving mean and variance for normalization
		# no gradients needed.
		self.bns = [
			torch.zeros(128, device=device),
			torch.ones(128, device=device),

			torch.zeros(256, device=device),
			torch.ones(256, device=device),

			torch.zeros(512, device=device),
			torch.ones(512, device=device),

			torch.zeros(1024, device=device),
			torch.ones(1024, device=device),

			torch.zeros(28 * 28, device=device),
			torch.ones(28 * 28, device=device),

		]


	def init_weight(self, vars=None):
		"""
		init vars and self.bns
		:param vars:
		:return:
		"""

		if vars is None:
			vars = self.vars

		vars_idx = 0

		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		weight, bias = vars[vars_idx + 2], vars[vars_idx + 3]
		weight.uniform_()
		bias.zero_()
		vars_idx += 4

		# zero mean and one variance.
		for i in range(len(self.bns) // 2 ):
			# mean
			self.bns[i].zero_()
			# variance
			self.bns[2 * i + 1].fill_(1)


	def forward(self, z, c, vars):
		"""

		:param z:
		:param c:
		:param vars:
		:return:
		"""

		vars_idx, bns_idx = 0, 0

		# [b, z_dim] + [b, c_dim] => [b, new_dim]
		x = torch.cat([z, c], dim=1)

		# [b, z+c] => [b, 128]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.1)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 128] => [b, 256]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.1)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 256] => [b, 512]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.1)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 512] => [b, 1024]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.1)

		x = F.leaky_relu(x, 0.2)
		vars_idx += 4
		bns_idx += 2

		# [b, 1024] => [b, 28*28]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		x = F.batch_norm(x, self.bns[bns_idx + 0], self.bns[bns_idx + 1],
		                 weight=vars[vars_idx + 2], bias= vars[vars_idx + 3],
		                 training=True, momentum=0.1)

		x = F.tanh(x)
		vars_idx += 4
		bns_idx += 2

		# reshape
		x = x.view(-1, 1, 28, 28)

		return x


class Discriminator:

	def __init__(self, n_class, device):

		# according to Pytorch w/b format, w = [out_dim, in_dim]
		# b = [out_dim]
		self.vars = [
			# [28*28, 512]
			torch.ones(512, 28 * 28, requires_grad=True, device=device),
			torch.zeros(512, requires_grad=True, device=device),
			# [512, 256]
			torch.ones(256, 512, requires_grad=True, device=device),
			torch.zeros(256, requires_grad=True, device=device),
			# [256, n]
			torch.ones(n_class, 256, requires_grad=True, device=device),
			torch.zeros(n_class, requires_grad=True, device=device)
		]



	def init_weight(self, vars=None):

		if vars is None:
			vars = self.vars

		vars_idx = 0

		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		vars_idx += 2


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		vars_idx += 2


		weight, bias = vars[vars_idx], vars[vars_idx + 1]
		stdv = 1. / math.sqrt(weight.size(1))
		weight.uniform_(-stdv, stdv)
		bias.uniform_(-stdv, stdv)
		# nn.init.xavier_uniform_(weight)
		# bias.data.fill_(0.01)
		vars_idx += 2




	def forward(self, x, vars):
		"""

		:param x: [b, 1, 28, 28]
		:param vars:
		:return:
		"""

		vars_idx = 0

		# [b, 1/2, 28, 28]
		x = x.view(x.size(0), -1)

		# [b, 28*28] => [b, 512]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		# x = self.bn1(x)
		x = F.leaky_relu(x, 0.2)
		vars_idx += 2

		# [b, 512] => [b, 256]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		# x = self.bn2(x)
		x = F.leaky_relu(x, 0.2)
		vars_idx += 2


		# [b, 256] => [b, n_class]
		x = F.linear(x, vars[vars_idx], vars[vars_idx + 1])
		# x = self.bn3(x)
		# here follow by CrossEntroyLoss
		# x = F.leaky_relu(x, 0.2)
		x = F.sigmoid(x)
		vars_idx += 2

		return x



def gan_main():

	from mnist_class import MNIST

	lr_d = 2e-4
	lr_g = 2e-4
	imagesz = 28
	batchsz_d = 64 # for fake data
	batchsz_g = 64 # for real data
	z_dim = 100
	n_class = 10

	device = torch.device('cuda')
	vis = visdom.Visdom()


	transform = transforms.Compose([transforms.Resize([imagesz, imagesz]),
	                                transforms.ToTensor(),
	                                transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))])
	# use self defined MNIST
	mnist = MNIST('data/mnist', class_idx=range(n_class), train=True, download=True, transform=transform)

	db = DataLoader(mnist, batch_size=batchsz_g, shuffle=True)
	db_iter = iter(db)

	c_dist = torch.distributions.categorical.Categorical(probs=torch.tensor([1/n_class] * n_class))

	g = Generator(z_dim, n_class, device)
	# only one realistic score needed.
	# input: 2*28*28
	d = Discriminator(1, device)
	# init is very important in case gradients==nan
	with torch.no_grad():
		g.init_weight()
		d.init_weight()
	g_vars, d_vars = g.vars, d.vars
	g_optim = optim.Adam(g_vars, lr=2e-4, betas=(0.5, 0.999))
	d_optim = optim.Adam(d_vars, lr=2e-4, betas=(0.5, 0.999))


	criteon = nn.MSELoss(size_average=True).to(device=device)

	for epoch in range(1000000):

		# [b, z]
		z = torch.rand(batchsz_d, z_dim).to(device)
		# [b] => [b, 1]
		y_hat = c_dist.sample((batchsz_d,) ).unsqueeze(1)
		# [b, 1] => [b, n_class]
		y_hat_oh = torch.zeros(batchsz_d, n_class).scatter_(1, y_hat, 1)
		# [b, 1] => [b]
		y_hat, y_hat_oh = y_hat.squeeze(1).to(device).float(), y_hat_oh.to(device).float()
		# print(y_hat, y_hat_oh)

		# [b, z+c] => [b, 1, 28, 28]
		x_hat = g.forward(z, y_hat_oh, g_vars)
		# [b, 1, 28, 28], we dnt use y_hat as we treat all x_hat as fake pairs.
		y_hat = y_hat.unsqueeze(1).unsqueeze(2).unsqueeze(3).expand(y_hat.size(0), 1, 28, 28)
		# concat [b, 1, 28, 28] => [b, 2, 28, 28]
		x_fake = torch.cat([x_hat, y_hat], dim=1)
		y_fake = torch.zeros(x_hat.size(0)).to(device)

		# [b, 1, 28, 28], [b]
		try:
			x, y = next(db_iter)
		except StopIteration as err:
			db_iter = iter(db)
			x, y = next(db_iter)
		x, y = x.to(device), y.to(device).float()
		# [b] => [b, 1] => [b, 1, 1] => [b, 1, 1, 1] => [b, 1, 28, 28]
		y = y.unsqueeze(1).unsqueeze(2).unsqueeze(3).expand(y.size(0), 1, 28, 28)

		# concat x with y
		# => [b, 2, 28, 28]
		x_real = torch.cat([x, y], dim=1)
		y_real = torch.ones(y.size(0)).to(device)

		# 1. train D
		# forward
		pred_real = d.forward(x_real, d_vars)
		pred_fake = d.forward(x_fake, d_vars)
		# compute real sample loss, [b] => [b, 1]
		loss_real = criteon(pred_real, y_real.unsqueeze(1))
		loss_fake = criteon(pred_fake, y_fake.unsqueeze(1))
		loss_d = ( loss_real + loss_fake ) / 2
		# backward
		d_grads = torch.autograd.grad(loss_d, d_vars)
		# d_vars = list(map(lambda p: p[0] - lr_d * p[1], zip(d_vars, d_grads)))
		for p, grad in zip(d_vars, d_grads):
			if p.grad is not None:
				p.grad.data = grad.detach()
			else:
				p.grad = grad.detach()
		d_optim.step()


		# 2. train G
		# [b, z]
		z = torch.rand(batchsz_d, z_dim).to(device)
		# [b] => [b, 1]
		y_hat = c_dist.sample((batchsz_d,) ).unsqueeze(1)
		# [b, 1] => [b, n_class]
		y_hat_oh = torch.zeros(batchsz_d, n_class).scatter_(1, y_hat, 1)
		# [b, 1] => [b]
		y_hat, y_hat_oh = y_hat.squeeze(1).to(device).float(), y_hat_oh.to(device).float()
		# print(y_hat, y_hat_oh)

		# forward
		# [b, z+c] => [b, 1, 28, 28]
		x_hat = g.forward(z, y_hat_oh, g_vars)
		y_hat = y_hat.unsqueeze(1).unsqueeze(2).unsqueeze(3).expand(y_hat.size(0), 1, 28, 28)
		# concat [b, 1, 28, 28] => [b, 2, 28, 28]
		x_real = torch.cat([x_hat, y_hat], dim=1)
		y_real = torch.ones(x_hat.size(0)).to(device)
		pred_real = d.forward(x_real, d_vars)
		loss_g = criteon(pred_real, y_real.unsqueeze(1))
		# backward
		g_grads = torch.autograd.grad(loss_g, g_vars)
		for p, grad in zip(g_vars, g_grads):
			if p.grad is not None:
				p.grad.data = grad.detach()
			else:
				p.grad = grad.detach()
		g_optim.step()





		if epoch % 200 == 0:

			# de-normalize, [b, 1, 28, 28]
			x_hat = x_hat[:20]
			# [b, 1, 28, 28]
			y_hat = y_hat[:20, 0, 0, 0]
			x_hat = x_hat * 0.5 + 0.5
			# print(x_hat[0,0,2])
			x_hat = F.upsample(x_hat, scale_factor=3)
			vis.images(x_hat, nrow=5, win='cgan-mnist-x-hat', opts={'caption':'cgan-mnist-x-hat'})
			vis.text(str(y_hat.detach().cpu().numpy()), win='cgan-mnist-y-hat', opts={'caption':'cgan-mnist-y-hat'})
			# print(pred[:20])
			# print(y[:20])

			x = x[:20]
			x = x * 0.5 + 0.5
			x = F.upsample(x, scale_factor=3)
			vis.images(x, nrow=5, win='cgan-mnist-x', opts={'caption':'cgan-mnist-x'})


			print('>>', epoch, loss_d, loss_g)

			print('D grads:', end=' ')
			for p in d_grads:
				print('%.4f'%p.norm().item(), end=' ')
			print(' G grads:')
			for p in g_grads:
				print('%.4f'%p.norm().item(), end=' ')
			print('')



def main():

	from mnist_class import MNIST

	lr_d = 0.01
	lr_g = 2e-4
	imagesz = 28
	batchsz_d = 100
	batchsz_g = 100
	z_dim = 100
	n_class = 10

	device = torch.device('cuda')
	vis = visdom.Visdom()


	transform = transforms.Compose([transforms.Resize([imagesz, imagesz]),
	                                transforms.ToTensor(),
	                                transforms.Normalize(mean=(0.5,), std=(0.5,))])
	# use self defined MNIST
	mnist = MNIST('data/mnist', class_idx=range(n_class), train=True, download=True, transform=transform)

	db = DataLoader(mnist, batch_size=batchsz_g, shuffle=True)
	db_iter = iter(db)

	c_dist = torch.distributions.categorical.Categorical(probs=torch.tensor([1/n_class] * n_class))

	g = Generator(z_dim, n_class, device)
	d = Discriminator(n_class, device)
	# # init is very important in case gradients==nan
	# with torch.no_grad():
	# 	g.init_weight()
	# 	d.init_weight()
	g_vars, d_vars = g.vars, d.vars
	# g_optim = optim.Adam(g_vars, lr=lr_g, betas=(0.5, 0.999))
	# d_optim = optim.Adam(d_vars, lr=2e-4, betas=(0.5, 0.999))

	# when using MSELoss, append F.sigmoid() at the end of D.
	# criteon = nn.CrossEntropyLoss(size_average=True).to(device)
	criteon = nn.MSELoss(size_average=True).to(device=device)
	criteon2 = nn.MSELoss(size_average=False).to(device=device)

	for epoch in range(1000000):

		# [b, z]
		z = torch.rand(batchsz_d, z_dim).to(device)
		# [b] => [b, 1]
		y_hat = c_dist.sample((batchsz_d,) ).unsqueeze(1)
		# [b, 1] => [b, n_class]
		y_hat_oh = torch.zeros(batchsz_d, n_class).scatter_(1, y_hat, 1)
		# [b, 1] => [b]
		y_hat, y_hat_oh = y_hat.squeeze(1).to(device), y_hat_oh.to(device)
		# print(y_hat, y_hat_oh)

		# [b, z+c] => [b, 1, 28, 28]
		x_hat = g.forward(z, y_hat_oh, g_vars)

		# 1. update D nets
		losses_d = []
		for i in range(10):
			pred = d.forward(x_hat, d_vars)
			l1 = criteon(pred, y_hat_oh)
			# MUST create_graph=True !!! to support 2nd derivate.
			d_grads = torch.autograd.grad(l1, d_vars, create_graph=True, retain_graph=True)
			# create new tensor, build computation graph
			d_vars = list(map(lambda p: p[0] - lr_d * p[1], zip(d_vars, d_grads)))

			losses_d += [l1.item()]



		# 2. update G nets

		# [b, 1, 28, 28], [b]
		try:
			x, y = next(db_iter)
		except StopIteration as err:
			db_iter = iter(db)
			x, y = next(db_iter)
		y_oh = torch.zeros(y.size(0), n_class).scatter_(1, y.unsqueeze(1).long(), 1)
		x, y, y_oh = x.to(device), y.to(device), y_oh.to(device)

		# d_vars is optimized weights.
		pred = d.forward(x, d_vars)
		l2 = criteon2(pred, y_oh)
		g_grads = torch.autograd.grad(l2, g_vars)

		"""
		1. no_grad keep or change requires_grad
		2. p = p - lr * grad, create new tensor, and if in no_grad, no grad 
		and also do NOT change initial array
		3. p = map(), create now tensor, no grad if no_grad()
		"""



		# with torch.no_grad():
		# 	# inplace, keep requires_grad info
		# 	for p, grad in zip(g_vars, g_grads):
		# 		p -= lr_g * grad

		g_vars = list(map(lambda p: p[0] - lr_g * p[1], zip(g_vars, g_grads)))

		# # memory leak
		# with torch.no_grad():
		# 	g_vars = map(lambda p: p[0] - lr_g * p[1], zip(g_vars, g_grads))
		# 	# MUST !!!
		# 	g_vars = list(map(lambda p: p.requires_grad_(), g_vars))

		# with torch.no_grad():
		# 	for p, grad in zip(g_vars, g_grads):
		# 		if p.grad is not None:
		# 			p.grad.copy_(grad.detach())
		# 		else:
		# 			p.grad = grad.detach()
		# # g_optim.zero_grad()
		# # l2.backward(retain_graph=True)
		# g_optim.step()




		# [b, n_class] => [b]
		pred = torch.argmax(pred, dim=1)
		correct = torch.eq(pred, y).sum().float()
		acc = correct.item() / np.prod(y.size()) # NOT y.sum()

		# print('>>>>memory check')
		# total_tensor, total_mem = 0, 0
		# for obj in gc.get_objects():
		# 	if torch.is_tensor(obj):
		# 		total_tensor += 1
		# 		total_mem += np.prod(obj.size())
		# 		print(obj.type(), obj.size())
		# print('<<<<', 'tensor:', total_tensor, 'mem:', total_mem//1024//1024)

		if epoch % 20 == 0:

			# de-normalize, [b, 1, 28, 28]
			x_hat = x_hat[:20]
			y_hat = y_hat[:20]
			x_hat = x_hat * 0.5 + 0.5
			# print(x_hat[0,0,2])
			x_hat = F.upsample(x_hat, scale_factor=3)
			vis.images(x_hat, nrow=5, win='toy-mnist-x-hat')
			vis.text(str(y_hat[:20].detach().cpu().numpy()), win='toy-mnist-y-hat')
			# print(pred[:20])
			# print(y[:20])

			x = x[:20]
			x = x * 0.5 + 0.5
			x = F.upsample(x, scale_factor=3)
			vis.images(x, nrow=5, win='toy-mnist-x')

			losses_d = np.array(losses_d).astype(np.float32)
			print('\nEpoch:', epoch, 'l2: %.4f'%l2.item(), ' acc:', acc)
			print(losses_d)

			print('D grads:', end=' ')
			for p in d_grads:
				print('%.4f'%p.norm().item(), end=' ')
			print('\nG grads:', end=' ')
			for p in g_grads:
				print('%.4f'%p.norm().item(), end=' ')

			print('\nD weight:', end=' ')
			for p in d_vars:
				print('%.4f' % p.norm().item(), end=' ')
			print('\nG weight:', end=' ')
			for p in g_vars:
				print('%.4f' % p.norm().item(), end=' ')
			print('')






if __name__ == '__main__':
	import argparse
	args = argparse.ArgumentParser()
	args.add_argument('-g', action='store_true', help='use gan to train')
	args = args.parse_args()

	if args.g:
		gan_main()
	else:
		main()
