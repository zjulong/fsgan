import  torch, time, os, pickle
import  numpy as np
import  torch.nn as nn
import  torch.optim as optim
from    torch.utils.data import DataLoader, TensorDataset
from    torchvision import datasets, transforms
import  visdom
from    torch.nn import functional as F



class Flatten(nn.Module):
	def __init__(self):
		super(Flatten, self).__init__()

	def forward(self, input):
		"""

		:param input: [b, dimx,...]
		:return: [b, -1]
		"""
		# print(input.shape)
		return input.view(input.size(0), -1)

class Generator(nn.Module):

	def __init__(self, z_dim=100, image_size=28, class_num=10):
		"""

		:param z_dim:
		:param image_size:
		:param class_num:
		"""
		super(Generator, self).__init__()

		self.z_dim = z_dim
		self.image_size = image_size
		self.class_num = class_num

		self.fc = nn.Sequential(
			# nn.Linear(self.z_dim + self.class_num, 1024),
			# nn.BatchNorm1d(1024),
			# nn.ReLU(),
			# nn.Linear(1024, 128 * (self.image_size // 4) * (self.image_size // 4)),
			nn.Linear(self.z_dim + self.class_num, 128 * (self.image_size // 4) * (self.image_size // 4)),
			nn.BatchNorm1d(128 * (self.image_size // 4) * (self.image_size // 4)),
			nn.ReLU(),
		)
		self.deconv = nn.Sequential(
			nn.ConvTranspose2d(128, 64, 4, 2, 1),
			nn.BatchNorm2d(64),
			nn.ReLU(),
			nn.ConvTranspose2d(64, 1, 4, 2, 1),
			nn.Tanh(),
		)

	def forward(self, z, label):
		"""

		:param input: [b, z-dim]
		:param label: [b, 10]
		:return:
		"""
		# [b, z] + [b, 10] => [b, z+10]
		x = torch.cat([z, label], dim=1)
		x = self.fc(x)
		x = x.view(-1, 128, (self.image_size // 4), (self.image_size // 4))
		x = self.deconv(x)

		return x


class Discriminator(nn.Module):

	def __init__(self, image_size=28, class_num=10):
		"""

		:param image_size:
		:param class_num:
		"""
		super(Discriminator, self).__init__()

		self.image_size = image_size
		self.class_num = class_num

		self.conv = nn.Sequential(
			nn.Conv2d(1, 64, 4, 2, 1),
			nn.LeakyReLU(0.2),
			nn.Conv2d(64, 128, 4, 2, 1),
			nn.BatchNorm2d(128),
			nn.LeakyReLU(0.2),
		)
		self.fc = nn.Sequential(
			nn.Linear(128 * (self.image_size // 4) * (self.image_size // 4), 1024),
			nn.BatchNorm1d(1024),
			nn.LeakyReLU(0.2),
			nn.Linear(1024, 10)
		)

	def forward(self, input, label):
		"""

		:param input: [b, c, h, w]
		:param label: [b, label, h, w]
		:return:
		"""

		x = torch.cat([input, label], 1)
		x = self.conv(x)
		x = x.view(-1, 128 * (self.image_size // 4) * (self.image_size // 4))
		x = self.fc(x)

		return x




class Classificator(nn.Module):

	def __init__(self, image_size=28, class_num=10):
		super(Classificator, self).__init__()


		self.model = nn.Sequential(
			nn.Conv2d(1, 64, kernel_size=3, padding=0),
			# nn.BatchNorm2d(64, momentum=1, affine=True),
			nn.ReLU(),
			nn.MaxPool2d(2),
			nn.Conv2d(64, 64, kernel_size=3, padding=0),
			# nn.BatchNorm2d(64, momentum=1, affine=True),
			nn.ReLU(),
			nn.MaxPool2d(2),
			nn.Conv2d(64, 64, kernel_size=3, padding=0),
			# nn.BatchNorm2d(64, momentum=1, affine=True),
			nn.ReLU(),
			nn.Conv2d(64, 64, kernel_size=3, padding=0),
			# nn.BatchNorm2d(64, momentum=1, affine=True),
			nn.ReLU(),

			# [b, 64, 1, 1]
			Flatten(),
			nn.Linear(64, class_num)
		)

		tmp1 = torch.Tensor(2, 1, image_size, image_size)
		tmp2 = self.model(tmp1)
		print(tmp1.shape, '=>', tmp2.shape)


	def forward(self, x):

		x = self.model(x)

		return x





def main():

	imagesz = 28
	batchsz2 = 10000
	class_num = 10
	sample_num = 10
	z_dim = 62
	K = 1 # inner update steps

	device = torch.device('cuda')


	transform = transforms.Compose([transforms.Resize([imagesz, imagesz]),
	                                transforms.ToTensor(),
	                                transforms.Normalize(mean=(0.5, ), std=(0.5,))])

	db = DataLoader(datasets.MNIST('data/mnist', train=True, download=True, transform=transform),
	                              batch_size=batchsz2, shuffle=True)

	G = Generator(z_dim=z_dim, image_size=imagesz, class_num=class_num).to(device)
	C = Classificator(image_size=imagesz, class_num=class_num).to(device)
	criteon = nn.CrossEntropyLoss().to(device)
	g_optim = optim.Adam(G.parameters(), lr=2e-3, betas=(0.5, 0.999))
	c_optim = optim.Adam(C.parameters(), lr=1e-3)


	model_parameters = filter(lambda p: p.requires_grad, G.parameters())
	params1 = sum([np.prod(p.size()) for p in model_parameters])
	model_parameters = filter(lambda p: p.requires_grad, C.parameters())
	params2 = sum([np.prod(p.size()) for p in model_parameters])
	print('G:', params1, 'C:', params2)


	vis = visdom.Visdom()


	for step in range(10000):

		# fixed noise & condition
		# [10, 1000, 62]
		sample_z = torch.rand([class_num, sample_num, z_dim]).to(device)

		# [10] => [10, 1] => [10, 1000]
		sample_y = torch.from_numpy(np.arange(class_num)).long().unsqueeze(1)\
						.expand(class_num, sample_num).to(device).contiguous()
		# scatter on dim=2, src=1, index=[10,1000,1]
		sample_y_oh = torch.zeros(class_num, sample_num, class_num).to(device).scatter_(2, sample_y.unsqueeze(2), 1)



		# [10, 1000, 62] => [10*1000, 62]
		# [10, 1000, 10] => [10*1000, 10]
		sample_z = sample_z.contiguous().view(-1, z_dim)
		sample_y_oh = sample_y_oh.contiguous().view(-1, class_num)
		# += => [10*1000, 1, 28, 28]
		# y: [10*1000]
		x_hat = G(sample_z, sample_y_oh)
		y_hat = sample_y.view(sample_num * class_num)



		demo = x_hat.detach().view(class_num, sample_num, 1, imagesz, imagesz)[:, 0, ...]
		# [10, 1, 28, 28]
		# print(demo.shape)
		demo = F.upsample(demo, scale_factor=3, mode='bilinear')
		vis.images(demo, nrow=class_num//2, win='gcn-xhat')


		# 1. update classificer
		C.train()
		pred = C(x_hat)
		loss_c = criteon(pred, y_hat)
		grads = torch.autograd.grad(loss_c, C.parameters())
		for g, w in zip(grads, C.parameters()):
			w = w - 1e-3 * g



		# 2.

		x, y = next(iter(db))
		x, y = x.to(device), y.to(device).long()
		pred = C(x)
		loss_g = criteon(pred, y)

		g_optim.zero_grad()
		res = torch.autograd.grad(loss_g, C.parameters())
		print('C', res[0].norm())
		res = torch.autograd.grad(loss_g, x_hat[0])
		print('x-hat', res[0].norm())
		res = torch.autograd.grad(loss_g, G.parameters())
		print('theta', res[0].norm())
		for p in G.parameters():
			print(p.grad.norm())
			break
		g_optim.step()

		print(step, loss_c.item(), loss_g.item())


















if __name__ == '__main__':
	main()