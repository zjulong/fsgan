import  torch, time, os, pickle
import  numpy as np
import  torch.nn as nn
import  torch.optim as optim
from    torch.utils.data import DataLoader, TensorDataset
from    torchvision import datasets, transforms
import  visdom
from    torch.nn import functional as F












def main():
	imagesz = 28
	batchsz2 = 10000
	class_num = 10
	sample_num = 10
	z_dim = 62
	K = 1 # inner update steps


	device = torch.device('cuda')


	transform = transforms.Compose([transforms.Resize([imagesz, imagesz]),
	                                transforms.ToTensor(),
	                                transforms.Normalize(mean=(0.5, ), std=(0.5,))])

	db = DataLoader(datasets.MNIST('data/mnist', train=True, download=True, transform=transform),
	                              batch_size=batchsz2, shuffle=True)

	x0 = torch.tensor(0.9, requires_grad=True)
	w = torch.tensor(1.0, requires_grad=True)
	b = torch.tensor(0.0, requires_grad=True)

	for i in range(10):
		pred = x0 * w + b
		l1 = (2 * x0 + 1 - pred)**2
		i, j = torch.autograd.grad(l1, [w, b])

		w = w - 1e-2 * i
		b = b - 1e-2 * j.norm()

		print(w, b)


	x1  = torch.tensor(2.0, requires_grad=False)
	pred = x0 * w + b
	l2 = (2 * x0 + 1 - pred)**2
	res = torch.autograd.grad(l2, x0)
	print(res)




if __name__ == '__main__':
	main()