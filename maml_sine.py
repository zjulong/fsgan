import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd
import random
import seaborn as sbs
from   tqdm import tqdm

# sbs.set_style('darkgrid')



class SineWaveTask:

	def __init__(self):
		self.a = np.random.uniform(0.1, 5.0)
		self.b = np.random.uniform(0, 2 * np.pi)
		self.train_x = None

	def f(self, x):
		return self.a * np.sin(x + self.b)

	def training_set(self, size=10, force_new=False):
		"""

		:param size:
		:param force_new: re-generate x or not.
		:return:
		"""
		if self.train_x is None and not force_new:
			self.train_x = np.random.uniform(-5, 5, size)
			x = self.train_x
		elif not force_new: # has train_x but force_new=Flase
			x = self.train_x
		else: # force_new no matter train_x existed.
			x = np.random.uniform(-5, 5, size)

		y = self.f(x)
		return torch.Tensor(x), torch.Tensor(y)

	def test_set(self, size=50):
		x = np.linspace(-5, 5, size)
		y = self.f(x)
		return torch.Tensor(x), torch.Tensor(y)

	def plot(self, *args, **kwargs):
		x, y = self.test_set(size=100)
		return plt.plot(x.numpy(), y.numpy(), *args, **kwargs)


def plot_db():

	SineWaveTask().plot()
	SineWaveTask().plot()
	SineWaveTask().plot()
	plt.show()

	for _ in range(1000):
		SineWaveTask().plot(color='green')
	plt.show()

	all_x, all_y = [], []

	for _ in range(10000):
		curx, cury = SineWaveTask().test_set(size=100)
		all_x.append(curx.numpy())
		all_y.append(cury.numpy())

	avg, = plt.plot(all_x[0], np.mean(all_y, axis=0))
	rand, = SineWaveTask().plot()
	plt.legend([avg, rand], ['Average', 'Random'])
	plt.show()





class MyModule(nn.Module):


	def params(self):
		"""
		in nn.Module, this is called parameters
		:return:
		"""
		return [p for _, p in self.named_params()]

	def named_leaves(self):
		return []

	def named_submodules(self):
		"""
		in nn.Module, this is named_modules/named_children
		:return:
		"""
		return []

	def named_params(self):
		subparams = []
		for name, mod in self.named_submodules():
			for subname, param in mod.named_params():
				# submodel_name.param_name
				subparams.append((name + '.' + subname, param))
		# extend with immediate leaves
		return self.named_leaves() + subparams

	def set_param(self, name, param):

		if '.' in name:
			# if this is a param from submodule
			n = name.split('.')
			module_name = n[0]
			rest = '.'.join(n[1:])
			for name, mod in self.named_submodules():
				if module_name == name:
					mod.set_param(rest, param)
					break
		else:
			setattr(self, name, param)

	def copy(self, other, same_var=False):
		"""
		copy from other.
		:param other:
		:param same_var:
		:return:
		"""
		for name, param in other.named_params():
			if not same_var:
				# only use .detach() will have bugs.!
				param = param.detach().clone()
				param.requires_grad_()
			self.set_param(name, param)




class GradLinear(MyModule):

	def __init__(self, *args, **kwargs):
		super().__init__()

		ignore = nn.Linear(*args, **kwargs)
		# self.weights = V(ignore.weight.data, requires_grad=True)
		# self.bias = V(ignore.bias.data, requires_grad=True)
		self.weights = ignore.weight.detach().clone()
		self.weights.requires_grad_()
		self.bias = ignore.bias.detach().clone()
		self.bias.requires_grad_()

	def forward(self, x):
		return F.linear(x, self.weights, self.bias)

	def named_leaves(self):
		return [('weights', self.weights), ('bias', self.bias)]


class SineModel(MyModule):

	def __init__(self):
		super().__init__()

		self.hidden1 = GradLinear(1, 40)
		self.hidden2 = GradLinear(40, 40)
		self.out = GradLinear(40, 1)

	def forward(self, x):
		x = F.relu(self.hidden1(x))
		x = F.relu(self.hidden2(x))
		return self.out(x)

	def named_submodules(self):
		return [('hidden1', self.hidden1), ('hidden2', self.hidden2), ('out', self.out)]



device = torch.device('cuda')

TRAIN_SIZE = 10000
TEST_SIZE = 1000
SINE_TRAIN = [SineWaveTask() for _ in range(TRAIN_SIZE)]
SINE_TEST = [SineWaveTask() for _ in range(TEST_SIZE)]

ONE_SIDED_EXAMPLE = None
while ONE_SIDED_EXAMPLE is None:
	cur = SineWaveTask()
	x, _ = cur.training_set()
	x = x.numpy()
	if np.max(x) < 0 or np.min(x) > 0:
		ONE_SIDED_EXAMPLE = cur




def sine_train(net, wave, optim=None, get_test_loss=False, create_graph=False, force_new=False):

	net.train()

	if optim is not None:
		optim.zero_grad()

	# generate training data
	# x: [10], y: [10]
	x, y = wave.training_set(force_new=force_new)
	x, y = x.to(device), y.to(device)
	# [b] => [b, 1]
	x, y = x.unsqueeze(1), y.unsqueeze(1)
	# scalar
	loss = F.mse_loss(net(x), y)
	# retain graph
	loss.backward(create_graph=create_graph, retain_graph=True)

	if optim is not None:
		optim.step()

	if get_test_loss:

		net.eval()
		# [50], [50]
		x, y = wave.test_set()
		x, y = x.to(device), y.to(device)
		x, y = x.unsqueeze(1), y.unsqueeze(1)

		loss_test = F.mse_loss(net(x), y)

		return loss.item(), loss_test.item()

	return loss.item()


def transfer_train(model, epochs=1):

	optim = torch.optim.Adam(model.params())

	print('train Transfer model.')

	for epoch in range(epochs):
		# shuffle SINE_TRAIN
		for task in random.sample(SINE_TRAIN, len(SINE_TRAIN)):
			loss = sine_train(model, task, optim)

		print(epoch, loss)

	print('done.')







def copy_sine_model(model):
	"""
	create a new model with same tensor value, differnet tensor reference.
	:param model:
	:return:
	"""
	m = SineModel().to(device=device)
	# same_var = Flase
	m.copy(model, same_var=False)

	return m


def evaluation(model, db, fits=(0, 1), lr=0.01):

	xtest, ytest = db.test_set()

	# copy a same model
	model = copy_sine_model(model)
	# Not sure if this should be Adam or SGD.
	optim = torch.optim.SGD(model.params(), lr)

	fit_res = []
	if 0 in fits: # [0, 1, 10]
		# [b, 1] => [b, 1]
		pred = model(xtest.unsqueeze(1))
		loss = F.mse_loss(pred, ytest.unsqueeze(1)).item()
		fit_res.append([0, pred, loss])

	for i in range(np.max(fits)): # range(10)

		# train on db.train_set()
		sine_train(model, db, optim)

		if i + 1 in fits:
			# i = 0
			# i = 9
			pred = model(xtest.unsqueeze(1))
			loss = F.mse_loss(pred, ytest.unsqueeze(1)).item()
			fit_res.append([ i + 1, pred, loss ])

	return fit_res


def plot_sine(model, test, fits=(0, 1), lr=0.01, title=''):

	plt.figure(title)

	xtest, ytest = test.test_set()
	xtrain, ytrain = test.training_set()

	fit_res = evaluation(model, test, fits, lr)

	train_plt, = plt.plot(xtrain.numpy(), ytrain.numpy(), '^')

	ground_truth_plt, = plt.plot(xtest.numpy(), ytest.numpy())

	plots = [train_plt, ground_truth_plt]
	legend = ['Training Points', 'Ground Truth']

	for n, pred, loss in fit_res:
		# n
		# res: [b, 1]
		# loss:
		pred = pred.squeeze(1).detach().numpy()
		cur, = plt.plot(xtest.numpy(), pred, '--')
		plots.append(cur)
		legend.append(f'After {n} Steps')

	plt.legend(plots, legend)






def plot_sine_loss(models_list, fits=(0, 1), lr=0.01, marker='s', linestyle='--', title=''):

	data = {
			'model': [],
	        'fits':  [],
	        'loss':  [],
	        'set':   []
	        }

	for name, models in models_list:

		if not isinstance(models, list):
			models = [models]

		for n_model, model in enumerate(models): # [1]

			for n_test, test in enumerate(SINE_TEST): # [1000 tasks]

				n_test = n_model * len(SINE_TEST) + n_test

				fit_res = evaluation(model, test, fits, lr)

				for n, _, loss in fit_res:
					data['model'].append(name)
					data['fits'].append(n)
					data['loss'].append(loss)
					data['set'].append(n_test)

	plt.figure(title)
	sbs.tsplot( pd.DataFrame(data), condition='model', value='loss',
		time='fits', unit='set', marker=marker, linestyle=linestyle)




def maml_train(model, epochs, lr_inner=0.01):

	optimizer = torch.optim.Adam(model.params())

	for epoch in tqdm(range(epochs)):

		# shuffle task
		for i, t in enumerate(random.sample(SINE_TRAIN, len(SINE_TRAIN))):

			new_model = SineModel().to(device)
			print(new_model.params()[0].is_cuda)
			new_model.copy(model, same_var=True)
			print(new_model.params()[0].is_cuda)

			sine_train(new_model, t, create_graph=True)

			for name, param in new_model.named_params():
				# if first_order:
				# 	grad = V(grad.detach().data)
				new_model.set_param(name, param - lr_inner * param.grad)

			sine_train(new_model, t, force_new=True)


			optimizer.step()
			optimizer.zero_grad()






def main():
	SINE_TRANSFER = SineModel().to(device)
	maml = SineModel().to(device)

	# transfer_train(SINE_TRANSFER, epochs=1)
	# plot_sine_test(SINE_TRANSFER, SINE_TEST[0], fits=[0, 1, 10], lr=0.02)


	# plot_sine_loss(
	# 	[('Transfer', SINE_TRANSFER), ('Random', SineModel())],
	# 	list(range(100)), marker='', linestyle='-', title='Transfer vs Random')

	maml_train(maml, 5)

	# learning.
	plot_sine_loss(
		[('Transfer', SINE_TRANSFER), ('MAML', maml), ('Random', SineModel().to(device))],
		list(range(10)), title='Tranfer vs MAML vs Random loss'
	)

	plot_sine(maml, SINE_TEST[0], fits=[0, 1, 10], lr=0.01, title='test MAML on sine')


	# # test
	# plot_sine(maml, ONE_SIDED_EXAMPLE, fits=[0, 1, 10], lr=0.01, title='test MAML on one side')
	#

	plt.show()

if __name__ == '__main__':
	main()