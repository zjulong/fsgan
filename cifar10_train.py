import  numpy as np
import  torch
from    torch.nn import functional as F
from    fsgan import FSGAN
from    cifar10 import CIFAR10
import  visdom







def main():
	batchsz = 8
	k = 8
	imgsz = 32


	cifar =  CIFAR10('db/', batchsz=batchsz, k=k, imgsz=imgsz, total_num=10000, train=True)
	# only sample 2 classes
	cifar_test =  CIFAR10('db/', batchsz=2, k=1, imgsz=imgsz, total_num=100, train=False)
	device = torch.device('cuda')
	gan = FSGAN().to(device)

	model_parameters = filter(lambda p: p.requires_grad, gan.parameters())
	params = sum([np.prod(p.size()) for p in model_parameters])
	model_parameters = filter(lambda p: p.requires_grad, gan.generator.parameters())
	params1 = sum([np.prod(p.size()) for p in model_parameters])
	model_parameters = filter(lambda p: p.requires_grad, gan.disc_rep.parameters())
	params21 = sum([np.prod(p.size()) for p in model_parameters])
	model_parameters = filter(lambda p: p.requires_grad, gan.disc_cmp.parameters())
	params22 = sum([np.prod(p.size()) for p in model_parameters])
	print('params:', params, '=', params1, '+', params21, '+', params22)

	vis = visdom.Visdom()

	# mnist.
	for epoch in range(10000):
		for step, (x, label) in enumerate(cifar):
			# [b, k, 1, 64, 64]
			# [b, k]
			x, label = x.to(device), label.to(device)
			# print(x.shape, label.shape)

			z = torch.randn(batchsz, k, 1, imgsz, imgsz).to(device)
			# [], [], [b, k, 1, imgsz, imgsz]
			loss_d, loss_g, x_hat = gan(x, z, label)

			if step % 200==0:
				print(step, loss_d.item(), loss_g.item())

				x_hat = x_hat.view(x_hat.size(0) * x_hat.size(1), 3, imgsz, imgsz)
				x = x.view(x.size(0) * x.size(1), 3, imgsz, imgsz)
				# re-normalize
				x_hat = 0.5 * x_hat + 0.5
				x = 0.5 * x + 0.5
				x_hat = F.upsample(x_hat, scale_factor=2, mode='bilinear')
				x = F.upsample(x, scale_factor=2, mode='bilinear')
				vis.images(x_hat, nrow=k, win='x_hat-cifar10', opts={'caption':'x_hat-train'})
				vis.images(x, nrow=k, win='x-cifar10', opts={'caption':'x-train'})

			if step % 600 == 0:
				x_test, label_test = cifar_test[0]
				x_test, label_test = x_test.to(device), label_test.to(device)

				x_hat = []
				for i in range(10):
					z = torch.randn(2, 1, 1, imgsz, imgsz).to(device)
					z = z + i/10
					# [2, 1, 3, 64, 64]
					x_hat.append(gan.generate(x_test, z))

				# [2, 1, 3, 64, 64] => [2, k, 3, 64, 64]
				x_hat = torch.cat(x_hat, dim=1)
				x_hat = x_hat.view(x_hat.size(0) * x_hat.size(1), 3, imgsz, imgsz)

				x_test = x_test.view(x_test.size(0) * x_test.size(1), 3, imgsz, imgsz)
				# re-normalize
				x_hat = 0.5 * x_hat + 0.5
				x_test = 0.5 * x_test + 0.5
				x_hat = F.upsample(x_hat, scale_factor=2, mode='bilinear')
				x_test = F.upsample(x_test, scale_factor=2, mode='bilinear')
				vis.images(x_hat, nrow=k, win='x_hat-test-cifar10', opts={'caption':'x_hat-test'})
				vis.images(x_test, nrow=1, win='x-test-cifar10', opts={'caption':'x-test'})



if __name__ == '__main__':
	main()