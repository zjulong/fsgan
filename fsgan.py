import  torch
from    torch import nn
from    torch import optim


class Flatten(nn.Module):
	def __init__(self):
		super(Flatten, self).__init__()

	def forward(self, input):
		"""

		:param input: [b, dimx,...]
		:return: [b, -1]
		"""
		# print(input.shape)
		return input.view(input.size(0), -1)



class FSGAN(nn.Module):


	def __init__(self, c_=3):
		super(FSGAN, self).__init__()
		factor = 16

		# 2x[b, 1, 64, 64] => [b, 1, 64, 64]
		self.generator = nn.Sequential(
			# nn.ConvTranspose2d(c_+1, factor * 8, kernel_size=3, stride=1, padding=1),
			# nn.BatchNorm2d(factor * 8),
			# nn.ReLU(inplace=True),

			nn.ConvTranspose2d(c_+1, factor * 4, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(factor * 4),
			nn.ReLU(inplace=True),

			nn.ConvTranspose2d(factor * 4, factor * 2, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(factor * 2),
			nn.ReLU(inplace=True),

			nn.ConvTranspose2d(factor * 2, factor * 1, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(factor * 1),
			nn.ReLU(inplace=True),

			nn.ConvTranspose2d(factor * 1, c_, kernel_size=3, stride=1, padding=1),
			# no batchnorm by DCGAN
			nn.Tanh(),
		)

		# [b, 1, 64, 64] => [b, 128, 11, 11]
		self.disc_rep = nn.Sequential(
			nn.Conv2d(c_, factor * 1, kernel_size=3, stride=2),
			# no batchnorm by DCGAN
			nn.LeakyReLU(inplace=True, negative_slope=0.2),

			nn.Conv2d(factor * 1, factor * 2, kernel_size=3, stride=1),
			nn.BatchNorm2d(factor * 2),
			nn.LeakyReLU(inplace=True, negative_slope=0.2),

			nn.Conv2d(factor * 2, factor * 4, kernel_size=3, stride=1),
			nn.BatchNorm2d(factor * 4),
			nn.LeakyReLU(inplace=True, negative_slope=0.2),

			# nn.Conv2d(factor * 4, factor * 8, kernel_size=3, stride=1),
			# nn.BatchNorm2d(factor * 8),
			# nn.LeakyReLU(inplace=True)
		)


		# feature map concatenate: 2 x [b, 128, 11, 11] => [b, 256, 11, 11]
		self.disc_cmp = nn.Sequential(
			nn.Conv2d(factor * 4 * 2, 32, kernel_size=3, stride=2),
			nn.BatchNorm2d(32),
			nn.ReLU(inplace=True),

			nn.Conv2d(32, 32, kernel_size=3, stride=3),
			nn.BatchNorm2d(32),
			nn.ReLU(inplace=True),

			# [b, 32, 1, 1]
			Flatten(),
			nn.Linear(32, 16),
			nn.ReLU(inplace=True),
			nn.Linear(16, 1),
			nn.Sigmoid()

		)


		# simply to indicate the current device context, ()
		self.non_tensor = nn.Parameter(torch.tensor(0))

		self.g_optim = optim.Adam(self.generator.parameters(), lr=5e-3, betas=(0.5, 0.999))
		self.d_optim = optim.Adam(list(self.disc_rep.parameters()) + list(self.disc_cmp.parameters()),
		                          lr=5e-3, betas=(0.5, 0.999))
		self.criteon = nn.MSELoss(size_average=True)



	def forward(self, x, z, y):
		"""

		:param x: [b, k, 1, h, w]
		:param z: [b, k, 1, h, w]
		:param y: [b, k]
		:return:
		"""
		batchsz, k, c_, h, w = x.size()
		assert h == w

		# 1. train D firstly
		# 2x[b, k, 1, h, w] => [b, k, 2, h, w]
		inp = torch.cat([x, z], dim=2)
		# [b, k, 2, h, w] => [b*k, 2, h, w] => [b*k, 1, h, w] => [b, k, 1, h, w]
		x_hat = self.generator(inp.view(batchsz * k, c_+1, h, h)).view(batchsz, k, c_, h, h)

		# [b, k, 1, h, w] => [1, b, k, 1, h, w] => [1, b, 1, k, 1, h, w] => [b, b, k, k, 1, h, w]
		x1 = x.unsqueeze(0).unsqueeze(2).expand(batchsz, batchsz, k, k, c_, h, h)
		# [b, k, 1, h, w] => [b, 1, k, 1, h, w] => [b, 1, k, 1, 1, h, w] => [b, b, k, k, 1, h, w]
		x2 = x.unsqueeze(1).unsqueeze(3).expand(batchsz, batchsz, k, k, c_, h, h)
		# [b, b, k, k, 1, h, w] => [b, b, k, k, 2, h, w] => [new_b, imgs]
		x_all = torch.cat([x1, x2], dim=4).view(batchsz * batchsz * k * k, 2 * c_, h, w)

		# [b, k] => [b, k, 1]
		y_ = y.unsqueeze(2)
		# [b, k, 1] => [1, b, k, 1] => [1, b, 1, k, 1] => [b, b, k, k, 1]
		y1 = y_.unsqueeze(0).unsqueeze(2).expand(batchsz, batchsz, k, k, 1)
		# [b, k, 1] => [b, 1, k, 1] => [b, 1, k, 1, 1] => [b, b, k, k, 1]
		y2 = y_.unsqueeze(1).unsqueeze(3).expand(batchsz, batchsz, k, k, 1)
		# eq [b, b, k, k, 1] => byte [b, b, k, k, 1] => long [new_b, 1]
		label_all = torch.eq(y1, y2).float().view(batchsz * batchsz * k * k, 1)

		# [b, k, 1, h, w] => [b, 1, k, 1, h, w] => [b, k, k, 1, h, w]
		x_n1 = x.unsqueeze(1).expand(batchsz, k, k, c_, h, h)
		# [b, k, 1, h, w] => [b, k, 1, 1, h, w] => [b, k, k, 1, h, w]
		x_n2 = x_hat.unsqueeze(2).expand(batchsz, k, k, c_, h, h)
		# [b, k, k, 2, h, w] => [new_b, imgs]
		x_n_fake = torch.cat([x_n1, x_n2], dim=3).view(batchsz * k * k, 2 * c_, h, h)
		# fake label for x_n_fake: [new_b, 1]
		label_n_fake = torch.zeros(batchsz * k * k, 1).to(self.non_tensor.device)

		# merge all data
		# [new_b, 2, h, w] with [new_b, 2, h, w] => [new_b, 2, h, w]
		x_all = torch.cat([x_all, x_n_fake], dim=0)
		# [new_b, 1] with [new_b, 1] => [new_b, 1] => [new_b]
		label_all = torch.cat([label_all, label_n_fake], dim=0).squeeze(1)


		# update batchsz <= new_b
		batchsz = x_all.size(0)
		# our Discriminator network is distinct from conventional D
		# x_all: [b, 2, h, w] => [b, 1, h, w] & [b, 1, h, w]
		x1, x2 = torch.chunk(x_all, 2, dim=1)
		# get feature maps of image pairs
		# [b, 1, 64, 64] => [b, 512, 9, 9]
		x1, x2 = self.disc_rep(x1), self.disc_rep(x2)
		# print(x1.shape)
		# concatenate x1 and x2
		# [b, 512, 9, 9] => [b, 1024, 9, 9]
		comb = torch.cat([x1, x2], dim=1)
		# get similarity scores
		# [b, 1024, 9, 9] => [b, 1] => [b]
		score = self.disc_cmp(comb).squeeze(1)
		# get loss of D
		# [b] vs [b]
		loss_d = self.criteon(score, label_all)
		# backprop
		self.d_optim.zero_grad()
		loss_d.backward()
		# print('>>d:')
		# for p in self.disc_rep.parameters():
		# 	print(p.norm())
		# for p in self.disc_cmp.parameters():
		# 	print(p.norm())
		self.d_optim.step()

		# 2. train G
		batchsz = x.size(0)
		# 2x[b, k, 1, h, w] => [b, k, 2, h, w]
		inp = torch.cat([x, z], dim=2)
		# [b, k, 2, h, w] => [b*k, 2, h, w] => [b*k, 1, h, w] => [b, k, 1, h, w]
		x_hat = self.generator(inp.view(batchsz * k, c_+1, h, h)).view(batchsz, k, c_, h, h)
		# [b, k, 1, h, w] => [b, 1, k, 1, h, w] => [b, k, k, 1, h, w]
		x_n1 = x.unsqueeze(1).expand(batchsz, k, k, c_, h, h)
		# [b, k, 1, h, w] => [b, k, 1, 1, h, w] => [b, k, k, 1, h, w]
		x_n2 = x_hat.unsqueeze(2).expand(batchsz, k, k, c_, h, h)
		# [b, k, k, 2, h, w] => [new_b, imgs]
		x_n_fake = torch.cat([x_n1, x_n2], dim=3).view(batchsz * k * k, 2 * c_, h, h)

		# update batchsz <= new_b
		batchsz = x_n_fake.size(0)
		# x_n_fake: [b, 2, h, w] => [b, 1, h, w]
		x1, x2 = torch.chunk(x_n_fake, 2, dim=1)
		# NOTICE: to make G improve, we need treat it as real image
		label = torch.ones(batchsz).to(self.non_tensor.device)
		# get features maps
		# [b, 512, 9, 9]
		x1, x2 = self.disc_rep(x1), self.disc_rep(x2)
		# concatenate x1 and x2
		# [b, 512, 9, 9] => [b, 1024, 9, 9]
		comb = torch.cat([x1, x2], dim=1)
		# get similarity scores
		# [b, 1024, 9, 9] => [b]
		score = self.disc_cmp(comb).squeeze(1)
		# compute loss of G
		loss_g = self.criteon(score, label)
		# backprop
		self.g_optim.zero_grad()
		loss_g.backward()
		# print('>>G:')
		# for p in self.generator.parameters():
		# 	print(p.norm())
		self.g_optim.step()


		return loss_d, loss_g, x_hat

	def generate(self, x, z):
		"""

		:param x:
		:param z:
		:return:
		"""
		batchsz, k, c_, h, w = x.size()

		# 1. train D firstly
		# 2x[b, k, 1, h, w] => [b, k, 2, h, w]
		inp = torch.cat([x, z], dim=2)
		# [b, k, 2, h, w] => [b*k, 2, h, w] => [b*k, 1, h, w] => [b, k, 1, h, w]
		x_hat = self.generator(inp.view(batchsz * k, c_ + 1, h, h)).view(batchsz, k, c_, h, h)

		return x_hat


def main():
	c_ = 3
	gan = FSGAN(c_=c_)

	x = torch.randn(4, c_, 64, 64)

	out1 = gan.disc_rep(x)
	out2 = gan.disc_rep(x)
	out = torch.cat([out1, out2], dim=1)

	out = gan.disc_cmp(out)
	print(out.shape)


	x = torch.randn(32, 2 * c_, 64, 64)
	out = gan.generator(x)
	print(out.shape)







if __name__ == '__main__':
	main()
