import  os, time
import  torch
import  argparse
from    fsgan_unet import FSGAN
from    mnist import MNIST
import  visdom
import  numpy as np
from    torch.nn import functional as F







def main(args):
	batchsz = 8
	k = 8
	imgsz = 32

	mnist =  MNIST('db/mnist', batchsz=batchsz, k=k, imgsz=imgsz, total_num=10000, train=True)
	# only sample 2 classes
	mnist_test =  MNIST('db/mnist', batchsz=2, k=k, imgsz=imgsz, total_num=100, train=False)
	device = torch.device('cuda')



	gan = FSGAN().to(device)
	print('Use U-Net Generator.')
	model_parameters = filter(lambda p: p.requires_grad, gan.parameters())
	params = sum([np.prod(p.size()) for p in model_parameters])
	model_parameters = filter(lambda p: p.requires_grad, gan.generator.parameters())
	params1 = sum([np.prod(p.size()) for p in model_parameters])
	model_parameters = filter(lambda p: p.requires_grad, gan.disc_rep.parameters())
	params21 = sum([np.prod(p.size()) for p in model_parameters])
	model_parameters = filter(lambda p: p.requires_grad, gan.disc_cmp.parameters())
	params22 = sum([np.prod(p.size()) for p in model_parameters])
	print('params:', params, '=', params1, '+', params21, '+', params22)

	vis = visdom.Visdom()

	# mnist.
	for epoch in range(1000):
		for step, (x, label) in enumerate(mnist):
			# [b, k, 1, 64, 64]
			# [b, k]
			x, label = x.to(device), label.to(device)
			# print(x.shape, label.shape)

			z = torch.randn(batchsz, k, 1, imgsz, imgsz).to(device)
			# [], [], [b, k, 1, imgsz, imgsz]
			loss_d, loss_g, x_hat = gan(x, z, label)

			if step % 40 == 0:
				print(step, loss_d.item(), loss_g.item())

				x_hat = x_hat.view(x_hat.size(0) * x_hat.size(1), 1, imgsz, imgsz)
				x = x.view(x.size(0) * x.size(1), 1, imgsz, imgsz)
				x_hat = F.upsample(x_hat, scale_factor=2, mode='bilinear')
				x = F.upsample(x, scale_factor=2, mode='bilinear')
				vis.images(x_hat, nrow=k, win='x_hat', opts={'caption':'x_hat-train'})
				vis.images(x, nrow=k, win='x', opts={'caption':'x-train'})

			if step % 200 == 0:
				x_test, label_test = mnist_test[0]
				x_test, label_test = x_test.to(device), label_test.to(device)
				z = torch.randn(2, k, 1, imgsz, imgsz).to(device)

				x_hat = gan.generate(x_test, z)

				x_hat = x_hat.view(x_hat.size(0) * x_hat.size(1), 1, imgsz, imgsz)
				x_test = x_test.view(x_test.size(0) * x_test.size(1), 1, imgsz, imgsz)
				x_hat = F.upsample(x_hat, scale_factor=2, mode='bilinear')
				x_test = F.upsample(x_test, scale_factor=2, mode='bilinear')
				vis.images(x_hat, nrow=k, win='x_hat-test', opts={'caption':'x_hat-test'})
				vis.images(x_test, nrow=k, win='x-test', opts={'caption':'x-test'})



if __name__ == '__main__':
	args = argparse.ArgumentParser()
	args.add_argument('-u', action='store_true', help='use u-net genereator')
	args = args.parse_args()

	main(args)