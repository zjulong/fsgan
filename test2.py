import  torch
from    torch import nn
from    torch import optim







def main():

	device = torch.device('cuda')
	lr = 5e-2

	w = torch.tensor(1., requires_grad=True).to(device)
	b = torch.tensor(0.1, requires_grad=True).to(device)


	criteon = nn.MSELoss()
	# optimizer = optim.Adam([w, b], lr=lr)

	for i in range(500):
		x = torch.rand(1).to(device)[0]


		pred = w * x + b
		y = 2 * x + 3
		loss = criteon(pred, y)

		grads = torch.autograd.grad(loss, [w, b])
		w = w - lr * grads[0]
		b = b - lr * grads[1]

		print(w, b)

def main2():

	device = torch.device('cuda')
	lr = 5e-2

	w_ = torch.tensor(1., requires_grad=True)
	b_ = torch.tensor(0.1, requires_grad=True)


	# with torch.no_grad():
	# 	print(w_, w)
	# 	w_.fill_(2)
	# 	print(w_, w)
	# 	w.fill_(3)
	# 	print(w_, w)
	# print(w_, w)

	criteon = nn.MSELoss()
	optimizer = optim.Adam([w_, b_], lr=lr)

	for i in range(500):
		# !!!
		w = w_.to(device)
		b = b_.to(device)
		x = torch.rand(1).to(device)[0]


		pred = w * x + b
		y = 2 * x + 3
		loss = criteon(pred, y)

		grads = torch.autograd.grad(loss, [w_, b_], retain_graph=True)
		# grads = torch.autograd.grad(loss, [w, b])
		with torch.no_grad():
			if w_.grad is not None:
				w_.grad.fill_(grads[0])
			else:
				w_.grad = grads[0]
			if b_.grad is not None:
				b_.grad.fill_(grads[1])
			else:
				b_.grad = grads[1]
		optimizer.step()

		print(w_, b_, loss.item())
		print(w, b, loss.item())

def main3():

	device = torch.device('cuda')
	lr = 5e-2

	w_ = torch.tensor(1., requires_grad=True)
	b_ = torch.tensor(0.1, requires_grad=True)

	criteon = nn.MSELoss()
	optimizer = optim.Adam([w_, b_], lr=lr)

	for i in range(500):
		x = torch.rand(1).to(device)[0]

		# !!!
		w = w_.to(device)
		b = b_.to(device)

		pred = w * x + b
		y = 2 * x + 3
		loss = criteon(pred, y)

		optimizer.zero_grad()
		loss.backward()
		optimizer.step()

		print(w, b)


if __name__ == '__main__':
	main3()