import  torch, time, os, pickle
import  numpy as np
import  torch.nn as nn
import  torch.optim as optim
from    torch.utils.data import DataLoader, TensorDataset
from    torchvision import datasets, transforms
import  visdom
from    torch.nn import functional as F








def main():

	lr_in = 1e-3
	lr_out = 1

	device = torch.device('cuda')
	vis = visdom.Visdom()

	# [2, 1]
	wg, bg = torch.tensor([[1.0], [1.0]], requires_grad=True).to(device), torch.tensor([1.0], requires_grad=True).to(device)
	# [1, 2]
	wd, bd = torch.tensor([[1.0, 1.0]], requires_grad=True).to(device), torch.tensor([1.0], requires_grad=True).to(device)
	criteon = nn.CrossEntropyLoss()
	# categorical dist [0 or 1]
	c_dist = torch.distributions.categorical.Categorical(probs=torch.tensor([0.5, 0.5]))


	def G(z_, c_, wg, bg):
		# [1, 2] x [2, 1] + [1]
		g_ = torch.matmul(torch.cat([z_, c_]).unsqueeze(0), wg) + bg
		g_ = F.relu(g_)
		return g_

	def D(g_, wd, bd):
		# [1, 1] x [1, 2] + [1]
		d_ = torch.matmul(g_, wd) + bd
		d_ = F.relu(d_)
		return d_

	vis.line(np.array([0]), np.array([0]), update=False, win='l2')
	for _ in range(10):
		# [1]
		z = torch.randn(1).to(device)
		# [1]
		c = c_dist.sample().unsqueeze(0).float().to(device)

		# [1, 1]
		x_hat = G(z, c, wg, bg)
		print('c:', c.item(), 'generated x:', x_hat.item())


	for epoch in range(1000000):
		z = torch.randn(1).to(device)
		# 0/1 => [0/1]
		c = c_dist.sample().unsqueeze(0).float().to(device)

		# [1, 1]
		x_hat = G(z, c, wg, bg)
		# if epoch%400==0:print('c', c.item(), 'x_hat:', x_hat)
	

		losses_d = []
		for i in range(5):
			pred_hat = D(x_hat, wd, bd)
			# [1, 2]
			# if epoch%400==0:print('c', c.item(), 'x_hat:', x_hat, 'pred_hat:', pred_hat)

			# 1. update d
			l1 = criteon(pred_hat, c.long())
			losses_d += [l1.item()]
			# if epoch%400==0:print('l1:', l1.item())
			# l1.backward()
			# with torch.no_grad():
			# 	wd -= 1e-3 * wd.grad # when use w = w - lr * grad, new_w has no grad information.
			# 	bd -= 1e-3 * bd.grad
			#
			# 	wd.grad.zero_()
			# 	bd.grad.zero_()
			# wd = wd - 1e-3 * wd.grad
			# bd = bd - 1e-3 * bd.grad
			grads = torch.autograd.grad(l1, [wd, bd], retain_graph=True, create_graph=True)
			wd = wd - lr_in * grads[0]
			bd = bd - lr_in * grads[1]
			# if epoch % 1000 == 0: print('grads for phi:', grads[0], grads[1])
	
		# 2. update g
		# [n, 1], uniform dist [0, 1]
		x = torch.rand(1000, 1).to(device)
		y = torch.ge(x, 0.5).squeeze(1).to(device)
		# if epoch%400==0:print('real x,y:', x.shape, y.shape)
	
		d = D(x, wd, bd)
		l2 = criteon(d, y.long())
		grads = torch.autograd.grad(l2, [wg, bg])
		wg = wg - lr_out * grads[0]
		bg = bg - lr_out * grads[1]

		if epoch%100==0:
			print('grads for phi:', losses_d)
			print('grads for theta:', grads[0], grads[1], '\nL2:', l2.item())
			vis.line(np.array([l2.item()]), np.array([epoch]), update='append', win='l2')
	
		if epoch%100==0:
			for _ in range(6):
				# [1]
				z = torch.randn(1).to(device)
				# [1]
				c = c_dist.sample().unsqueeze(0).float().to(device)

				# [1, 1]
				x_hat = G(z, c, wg, bg)
				print('c:', c.item(), 'generated x:', x_hat.item())














if __name__ == '__main__':
	main()